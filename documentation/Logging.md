# Logging

Logging is provided through the `BLRE::LogFactory` and uses the [spdlog] library under the hood. By default, all logs are written to `BlacklightDirectory>/FoxGame/Logs/BLRevive-<timestamp>.log`. 

BLRevive provides a new interface (`BLRE::structured_logger`) which supports structured logging features. See *structured logging* for more infos.

To get an overview of the methods provided with the log instances that the factory creates look at the [Basic Usage](https://github.com/gabime/spdlog#basic-usage) of spdlog docs.

## structured logging

The overall interface stays the same but adds the opportunity to provide additional data to a log either for a logger instance or single log entry.

### single log entry

Simply add `({ {key, val} })` after the log call to add data to the entry.

```cpp
auto log = BLRE::BLRevive::GetInstance()->Log;

// write a log entry with additional data
log->info("Hello World")({
    {"My Name", "Slim Shady"},
    {"RapGod", true}
});

// cache a log message with data for later use
auto logEntry = log->debug("some info")({{"debug": true}});

// do something else

// append more data and write cached log message
logEntry({{"more", "data"}});

// output in log:
// [01:44:15.259.259971] [  info  ] [ BLRevive ] Hello World
//      My Name = Slim Shady
//      RapGod = true
// [01:44:15.259.259971] [  debug ] [ BLRevive ] some info
//      debug = true
//      more = data
```

The `key` needs to be a string and `value` accepts all types which are stringable.

### logger instance

Use `BLRE::structured_logger::set_context` and `BLRE::structured_logger::add_context` to provide context data to all log entries for a logger instance.

```cpp
// create a new logger instance
auto logger = BLRE::BLRevive::GetInstance()->LogFactory->Create("MyLogger");

// set data which is provided to all log entries done with this instance
logger->set_context({
    {"Name", "Homer"},
    {"ID", 200}
});

// write log entries
logger->info("this is an info");

// add more data
logger->add_context({{"Base", "Springfield"}});
logger->info("more infos");

// combine with single log data
logger->error("this is a huge error")({
    {"ErrorID": 0x2E4 },
    {"Message": "this shouldnt happen!"}
});

// reset data for logger
logger->reset_context();
```

This will output the following in the log file:

```
[01:44:15.259.259971] [  info  ] [ MyLogger ] this is an info
    Name = Homer
    ID = 200
[01:44:15.259.259971] [  info  ] [ MyLogger ] more infos
    Name = Homer
    ID = 200
    Base = Springfield
[01:44:26.707.707118] [ error  ] [ MyLogger ] this is a huge error
    Name = Homer
    ID = 200
    ErrorID = 0x2E4
    Message = this shouldnt happen!
```


## global log

It is  recommended to use the log instances provided with the [components](Components.md). However there are situations where logging outside the scope of components is needed (global functions, static methods, ...). 

In that case the `MainLog` macro defined inside `BLRevive/BLRevive.h` should be used:

```cpp
#include <BLRevive/BLRevive.h>

static void GlobalFuunction()
{
    MainLog->info("An info from a function that does not belong to a component!");
}
```

## custom log instances

For more advanced scenarios a log instance with custom configuration can be retrieved from the `BLRE::BLRevive::LogFactory` instance. 

```cpp
#include <BLRevive/BLRevive>

void MyCustomLogger()
{
    // get a custom log instance from the LogFactory
    auto customLogger = BLRE::BLRevive::GetInstance()->LogFactory->Create("CustomLogger", spdlog::level::debug, "CustomFileName");

    // configure the logger (using spdlog interface)
    customLogger->set_pattern("%v");

    // this will create a custom log file at <BlacklightDirectory>/FoxGame/Logs/CustomFileName-<timestamp>.log
    customLogger->info("A custom info log");
}
```