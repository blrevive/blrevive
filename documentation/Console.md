# Console

> The console interface is currently in beta status and its API will change very likely with future releases. Also, not all functionalities documented here are implemented.

The `BLRE::UI::Console` class provides a simple interface to inject custom commands that can be invoked at runtime through the console window.

This is especially useful for server instances and debugging.

## Menu

A menu (`BLRE::UI::Menu`) is a collection of commands.

```cpp
#include <BLRevive/BLRevive.h>

void AddCustomMenu()
{
    auto console = BLRE::BLRevive::GetInstance()->Console;

    auto menu = new BLRE::UI::Menu();
    menu->AddCommand("AwesomeCommand", 
        [](std::string args)
        {
            std::cout << args << std::endl;
        }
    );

    console->Root->AddMenu("MyAwesomeMenu", menu);
}

```