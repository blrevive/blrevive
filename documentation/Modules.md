# Modules

Modules are libraries which contain features to extend BL:R or override native logics in order to make it playable.

They are the most important part of this project as they will provide features that will care about various parts of the game, such as handling loadouts ([BLRevive/Modules/Loadout Manager](https://gitlab.com/blrevive/modules/loadout-manager)), user profiles and settings ([BLRevive/Modules/Settings Manager](https://gitlab.com/blrevive/modules/settings-manager)) and integration of servers with our discord ([BLRevive/Modules/Server Util](https://gitlab.com/blrevive/modules/server-util)).

> For a full list of available modules look into the [BLRevive/Modules](https://gitlab.com/blrevive/modules) group.

## Manager

The module manager (`BLRE::Modules::Manager`) is responsible for loading and managing modules at runtime. 

At startup the manager will load and initialize all modules defined in the `BLRevive.Modules` key of the configuration.

## Types

Modules are seperated into those who should be loaded with server instance (`BLRE::Modules::ModuleType::Server`) or client instances (`BLRE::Modules::ModuleType::Client`).

- modules of `ModuleType::Server` should be listed in the `BLRevive.Modules.Server` array of config
- modules of `ModuleType::Client` should be listed in the `BLRevive.Modules.Client` array of config

## Develop Modules

It is highly recommended to use the [BLRevive/Modules/Skeleton](https://gitlab.com/blrevive/modules/skeleton) as a template to initialize custom modules (just follow the steps in the docs of the repository).

It is also recommended to store the modules at GitLab as the skeleton provides strong CI configuration and automated releases.

> You are welcome to create modules in the [BLRevive/Modules](https://gitlab.com/blrevive/modules) subgroup, just ask for permissions at our discord server.