# User Authentication

User authentication is available with the help of the SteamAPI and `UserAuth` class.

## How it works

BL:R already ships with an integration of the steam api and has native functions to retrieve session tickets, send them to the server and validate them there.

- `SteamAPI_Init` is called on startup of the client which initializes the SteamAPI and will fail when steam is not open or the user is not logged in
- `AFoxPC::ClientReadySteamAuthTicket` gets called when connecting to the server and marks the client ready for authentication
- `AFoxPC::SendAuthenticationTicket` gets a steam session ticket using the api and sends it to the server through replication
- `AFoxPC::ServerAuthenticateTicket` gets called on the server through replication, which is detoured to `UserAuth::ServerAuthenticateTicket` by BLRevive
- `UserAuth::ServerAuthenticateTicket` will validate the steam session ticket and check if the user is banned.
    - if the ticket is invalid or the user is on the `AAccessControl::BannedIDs` list the player will be force kicked
    - otherwise nothing special happens

## Setup for hosters

Since game server hosters normaly don't have a steam client installed on the hosting machine, it is mandatory to copy `steamclient.dll` and `Steam.dll` from the steam installation directory into `<BLR>/Binaries/Win32`.

The authentication can be disabled by setting `Proxy.Server.UserAuthentication` to false in the BLRevive config.
