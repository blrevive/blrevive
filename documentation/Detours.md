# Detours

The library provides an easy way to hijack into the Unreal Engine workflow in order to inject custom logic with the `BLRE::Detours::UFunction` class.

The `BLRE::Detours::ProcessEvent` and `BLRE::Detours::CallFunction` are mainly for debugging purposes.

## `UFunctionHook`s

Hook into `UFunction`s to retrieve or manipulate parameters and/or inject custom logic into game functions.

This interface should be used for the following use cases:

1. retrieve parameters of a `UFunction` (use `UFunctionHook::HookPre`)
1. retrieve output parameters or result of a `UFunction` (use `UFunctionHook::HookPost`)
1. manipulate output parameters or result of a `UFunction` (use `UFunctionHook::HookPost`)

To overwrite the behavior of a `UFunction` the `UFunctionDetour` interface should be used.

```cpp
#include <BLRevive/BLRevive.h>
#include <SdkHeaders.h>

void BeforeTimer(UObject* Object, FFrame& Stack, void* const Result)
{
    // get the calling object
    auto objGriTdm = (AFoxGRI_TDM*)Object;

    // get called function
    auto fnTimer = (UFunction*)Stack.Node;

    // get function parameters
    auto fnParams = (AFoxGRI_TDM_eventTimer_Params*)Stack.Locals;

    MainLog->debug("Pre: FoxGRI_TDM::Timer invoked");
    
    // return true will remove the current hook
    return false;
}


void Detour_FoxGRI_TDM_Timer()
{
    // get instance of function hook manager
    auto funcHook = BLRE::BLRevive::GetInstance()->FunctionHook;

    // invoke a callback (BeforeTimer) before a UFunction (FoxGRI_TDM.Timer) is executed
    funcHook->HookPre("FoxGame.FoxGRI_TDM.Timer", BeforeTimer);

    // invoke a callback (lambda) after a UFunction (FoxGRI_TDM.Timer) was executed
    funcHook->HookPost("FoxGame.FoxGRI_TDM.Timer", 
        [](UObject* Object, FFrame& Stack, void* const Result)
        {
            MainLog->debug("Post: FoxGRI_TDM::Timer executed");

            // this will remove the post-hook of FoxGame.FoxGRI_TDM.Timer
            return true;
        }
    );
}
```

## `UFunctionDetour`

Detour `UFunction`s to overwrite game logic.

### argument mapping

The `UFunctionDetour` provides an interface which is capable of mapping a function to `UFunction`. The mapping is based on the following rules:

| detour function signature | `UFunction` signature |
|---|---|
| `TReturnType Function(TClass*)` | `TReturnType TClass::Function()` |
| `TReturnType Function(TClass*, TParams*) ` | `TReturnType TClass::Function(TParams...)` |
| `TReturnType TClass::Function()` | `TReturnType TClass::Function()`|
| `TReturnType TClass::Function(TParams*)` | `TReturnType TClass::Function(TParams...)` |

### usage example (loose functions)

```cpp
#include <BLRevive/BLRevive.h>
#include <SdkHeaders.h>
#include <spdlog/fmt/fmt.h>

FString AFoxGame_GetRandomBotName(AFoxGame* fg)
{
    return "Hello Bot";
}

FString AFoxGame_ConvertUniqueIdToString(AFoxGame* fg, AFoxGame_execConvertUniqueIdToString_Parms* parms)
{
    return fmt::format("{}:{}", parms->InUniqueId.Uid.A, parms->InUniqueId.Uid.B);
}


void DetourFoxGameFunctions()
{
    auto foxGame = UObject::GetInstanceOf<AFoxGame>();
    auto funcDetour = BLRE::BLRevive::GetInstance()->FunctionDetour;

    // attach our AFoxGame_GetRandomBotName detour to the `UFunction`
    funcDetour->Attach("FoxGame.FoxGame.GetRandomBotName", AFoxGame_GetRandomBotName);

    // call our detoured function by class 
    auto randomName = foxGame->GetRandomBotName();
    assert(strcmp(randomName.ToChar(), "Hello Bot") == 0); // == true

    // detach (= remove) our detour and reset UFunction to its original state
    funcDetour->Detach("FoxGame.FoxGame.GetRandomBotName");

    // attach our AFoxGame_ConvertUnqiueIdToString (with params) detour to the UFunction
    funcDetour->Attach("FoxGame.FoxGame.ConvertUniqueIdToString", AFoxGame_ConvertUniqueIdToString);

    // call our detoured function with params
    auto uniqueIdStr = foxGame->ConvertUniqueIdToString({200, 400});
    assert(strcmp(uniqueIdStr.ToChar(), "200:400")); // = true

    funcDetour->Detach("FoxGame.FoxGame.ConvertUniqueIdToString");
}
```

### usage example (member functions)

```cpp
#include <BLRevive/BLRevive.h>
#include <SdkHeaders.h>
#include <spdlog/fmt/fmt.h>

class CustomFoxGame : public AFoxGame
{
    FString GetRandomBotName()
    {
        return "Hello Bot";
    }

FString ConvertUniqueIdToString(AFoxGame_execConvertUniqueIdToString_Parms* parms)
    {
        return fmt::format("{}:{}", parms->InUniqueId.Uid.A, parms->InUniqueId.Uid.B);
    }
}

void DetourFoxGameFunctions()
{
    auto foxGame = UObject::GetInstanceOf<AFoxGame>();
    auto funcDetour = BLRE::BLRevive::GetInstance()->FunctionDetour;

    funcDetour->Attach("FoxGame.FoxGame.GetRandomBotName", &CustomFoxGame::GetRandomBotName);
    
    // call our detoured function by class 
    auto randomName = foxGame->GetRandomBotName();
    assert(strcmp(randomName.ToChar(), "Hello Bot") == 0); // == true

    // detach (= remove) our detour and reset UFunction to its original state
    funcDetour->Detach("FoxGame.FoxGame.GetRandomBotName");

    funcDetour->Attach("FoxGame.FoxGame.ConvertUniqueIdToString", &CustomFoxGame::ConvertUniqueIdToString);

    // call our detoured function with params
    auto uniqueIdStr = foxGame->ConvertUniqueIdToString({200, 400});
    assert(strcmp(uniqueIdStr.ToChar(), "200:400")); // = true

    funcDetour->Detach("FoxGame.FoxGame.ConvertUniqueIdToString");
}
```

## `UObject::ProcessEvent`

> This detour is meant for debugging purposes only as it heavely decreases performance of the application.

This detour is useful to understand what events will be processed in which order and debug your custom logics. The invocations of `ProcessEvent` will be logged to the `<BlacklightDirectory>/FoxGame/Logs/Debug-<timestamp>.log` file.

If the library is compiled as debug target, you can set `LogProcessEvent: true` in the [debug configuration](../Readme.md#debug-configuration) which will attach this detour and log event calls.

## `UObject::CallFunction`

> This detour is meant for debugging purposes only as it heavely decreases performance of the application.

This detour is useful to trace function calls for debugging your custom logic. The invocations of `CallFunction` will be logged to the `<BlacklightDirectory>/FoxGame/Logs/Debug-<timestamp>.log` file.

If the library is compiled as debug target, you can set `LogCallFunction: true` in the [debug configuration](../Readme.md#debug-configuration) which will attach this detour and log event calls.