# Components

A component encapsulates logic for a specific part of the program and is associated with a name.

Components can be defined by inheriting from `BLRE::Component`.

## Example

```cpp
#include <BLRevive/Component.h>

class MyAwesomeComponent : public Component
{
public:
    MyAwesomeComponent()
        // the component constructor takes the name as first param
        : Component("AwesomeComponent") {};
    
    MyAwesomeComponent(std::string customLogFileName)
        // optionally a second parameter can be passed 
        //  to define a custom log file name this component should write to
        : Component("AwesomeComponent", customLogFileName) {};

    void AwesomeLogs();
}
```


## Logging

Every component will have its own log instance (`BLRE::Component::Log`) which automaticaly applies the name of the component as scope when an instance of the component is created.

*Based on the example component class above:*
```cpp
void MyAwesomeComponent::AwesomeLogs()
{
    Log->debug("An awesome debug message from an awesome component!");
    Log->info("Component::Log is an spdlog::logger interface");
}
```