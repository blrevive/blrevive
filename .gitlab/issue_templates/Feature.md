## Summary

> Summerize the feature request.

## Use Case

> Explain why this feature request is necessary.

## (optional) Proposal

> Make suggestions for the implementation of that feature request. If possible, include screenshots for GUI changes.

## (optional) Requirements

> **Only for developers**: List all requirements that comes with implementing this feature.
