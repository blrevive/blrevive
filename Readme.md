# BLRevive 

A library framework for Blacklight: Retribution that is used to unlock it for the PC platform. 

## Features

- easy detouring of [`UFunction`s](documentation/Detours.md#ufunctions)
- extendable using [modules](documentation/Modules.md)
    - find modules at [BLRevive/Modules] group
    - create a module using [BLRevive/Modules/Skeleton]
- simple [console](documentation/Console.md) supporting custom commands
- [debugging configuration](#debug-configuration)
- [user authentication](documentation/Components/UserAuth.md) using Steam

## Documentation

Detailed documentation and examples are available at [https://blrevive.gitlab.io/blrevive/latest](https://blrevive.gitlab.io/blrevive/latest).

> The docs are auto-generated using Doxygen and Gitlab CI.

## Install

The recommended way to install this library is the [BLRedit] launcher but it can also be done manually.

### using BLRedit

Simply go to the [BLRedit] github page, download the latest release and follow their install instructions.

### manual steps

- download the latest (or desired) [release](https://gitlab.com/blrevive/blrevive/-/releases)
    - BLRevive.dll
    - DINPUT8.dll
- copy `BLRevive.dll` and `DINPUT8.dll` to the `<BlacklightDirectory>/Binaries/Win32` directory

## Build

This project uses the CMake toolset along with CPM for easier dependency management.

> The commands below need to be executed from a shell which has access to the CMake toolset. Make sure [Visual Studio] with CMake is installed and use the Visual Studio Developer Powershell to execute commands.

- `cmake -A Win32 -B build <options>`
- either
    - use Visual Studio to open/compile the generated `build/BLRevive.sln`
    - run `cmake --build build/`

### cmake options

| option | description | default |
|---|---|---|
| `BLR_DIRECTORY` | absolute path to BL:R directory | `C:\\Program Files (x86)\\Steam\\steamapps\\common\\blacklightretribution` |
| `BLR_EXECUTABLE` | filename of BL:R application | `FoxGame-win32-Shipping.exe` |
| `CPM_<dep>_SOURCE` | absolute path to a local version of a dependency (see [CPM](https://github.com/cpm-cmake/CPM.cmake#local-package-override) docs for more details)| - |

**Example**: Assuming BL:R is located at `C:/BLR/3.02` and a local version of the [SDK] exists at `C:/BLR/SDK`, you should use the following build command: `cmake -A Win32 -B build -DBLR_DIRECTORY="C:/BLR/3.02" -DCPM_sdk_SOURCE="C:/BLR/SDK"`

## Develop

> For features that are not required/useful for other modules consider creating your own module using the [BLRevive/Modules/Skeleton].

Help in development is very much appreciated but please follow the rules below to ensure a good collaboration:

- work on your own fork of this project and use merge requests
- always work on the `development` branch (or a sub-branch of that)
- try to make small and descriptive commits (use [angular commit convention] so they are recognized by our automatic semantic releases)

### debugging

The generated VS solution is configured to support debugging easely.

If this library is build for debug target it will prompt the user with the following questions before BL:R starts:

- wether to start a server or client instance
- the [URL parameters](https://docs.unrealengine.com/udk/Three/CommandLineArguments.html#Map/Server/Editor%20URL%20Parameters) that should be passed

#### debug configuration

After the first start in debug mode, the library creates a debugging configuration file at `<BlacklightDirectory>/FoxGame/Config/BLRevive/debug.json` which can be used to configure debugging features.

| config key | description | default |
|---|---|---|
| `Client.URL` | default URL for client instances | `127.0.0.1?Name=AwesomeDev` |
| `Server.URL` | default URL for server instances | `HeloDeck?NumBots=12` |
| `LogCallFunction` | log all invokes of `UObject::CallFunction` (writes to `<BlacklightDirectory>/FoxGame/Logs/Debug-<timestamp>.log`) | `false` |
| `LogProcessEvent` | log all invokes of `UObject::ProcessEvent` (writes to `<BlacklightDirectory>/FoxGame/Logs/Debug-<timestamp>.log` | `false` |

[BLRedit]: https://github.com/HALOMAXX/BLREdit
[Visual Studio]: https://visualstudio.microsoft.com/vs/community/
[SDK]: https://gitlab.com/blrevive/tools/sdk
[BLRevive/Modules/Skeleton]: https://gitlab.com/blrevive/modules/skeleton
[angular commit convention]: https://www.conventionalcommits.org/en/v1.0.0-beta.4/
[BLRevive/Modules]: https://gitlab.com/blrevive/modules