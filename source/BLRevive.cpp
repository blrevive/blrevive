#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>
#include <iostream>
#include <functional>

#include <BLRevive/BLRevive.h>
#include <BLRevive/Config.h>
#include <BLRevive/Utils.h>
#include <BLRevive/Components/Modules.h>
#include <BLRevive/Components/UserAuth.h>

#include <httplib.h>
#include <BLRevive/Components/Patcher.h>

#ifdef DEBUG
#include <debugapi.h>
#include <BLRevive/DebugUtils.h>
#endif

#define WRITE_DUMP_FILE_ADDR 0x9F01D0
typedef void(__fastcall *tWriteDumpFile)(int param);
tWriteDumpFile oWriteDumpFile = nullptr;

struct UnrealExceptionMsg
{
	unsigned char pad[0x20];
	const wchar_t* Message;
};

struct UnrealException
{
	UnrealExceptionMsg* Msg;
};

void __fastcall hkWriteDumpFile(UnrealException* ex)
{
	// convert error message from wchar to char
	char* msg = (char*)calloc(1, 1024);
	size_t num = 0;
	if(ex->Msg->Message != nullptr)
		wcstombs_s(&num, msg, (size_t)1024, ex->Msg->Message, (size_t)1023);

	// log exception and exit
	MainLog->critical("UnrealException: {}", msg);
	exit(0);
}

BLRE::BLRevive* BLRE::BLRevive::Create()
{
	if(_Instance == nullptr)
		_Instance = new BLRevive();

	return _Instance;
}

BLRE::BLRevive* BLRE::BLRevive::GetInstance()
{
	return _Instance;
}

void BLRE::BLRevive::Initialize()
{
	bool bConsoleAttached = false;
	if (Utils::IsServer()) {
		BLRE::UI::Console::AttachOrCreate();
		bConsoleAttached = true;
	}

	URL = BLRE::Utils::URL::Parse(GetCommandLineA());

	// initialize config
	std::string configFilePath = std::string(Utils::FS::BlreviveConfigPath()) + URL.GetParam("config", "default") + ".json";
	std::string configFileErr = "";
	try {
		Config = BLRE::Config::Load(configFilePath, true);
		map_url_config(Config, URL);
	} catch (file_not_found& ex) {
		configFileErr = fmt::format("failed to read config: \n\t{}", ex.what());
		if (bConsoleAttached) {
			spdlog::error(configFileErr);
			spdlog::info("using default configuration");
		}
	} catch (nlohmann::json::exception& ex) {
		configFileErr = fmt::format("failed to parse config: \n\t{}", ex.what());
		if (bConsoleAttached) {
			spdlog::error(configFileErr);
			spdlog::info("using default configuration");
		}
	}

	// initialize logging
	LogFactory = new BLRE::LogFactory();

	if (Config.Logger.Level != LogLevel::none) {
		LogFactory->SetDefaultLevel(Config.Logger.Level);
	} else {
		Config.Logger.Level = DEFAULT_LOG_LEVEL;
	}
	LogFactory->SetDefaultFile(Config.Logger.FilePath);
	LogFactory->SetLogTarget(Config.Logger.Target);

	Log = LogFactory->Create("BLRevive", Config.Logger.Level, Config.Logger.FilePath);
	Log->info("Initializing BLRevive");

	if (!configFileErr.empty()) {
		Log->error(configFileErr);
		Log->info("using default configuration");
	}


	// detour dump file to prevent creation of big dump files on disk
	oWriteDumpFile = (tWriteDumpFile)((char*)GetModuleHandleA(nullptr) + WRITE_DUMP_FILE_ADDR);
	auto writeDumpDetour = new Detours::DetourBase((uintptr_t)oWriteDumpFile, hkWriteDumpFile, "WriteDumpFileDt");
	writeDumpDetour->Attach();

	// attach detours for debug purposes
#ifdef DEBUG
	bool isServer = Utils::IsServer();
	auto debugConfig = BLRE::DebugUtils::GetDebugConfig();

	// create UObject::ProcessEvent detour
	if ((isServer && debugConfig.Server.LogProcessEvent) || (!isServer && debugConfig.Client.LogProcessEvent)) {
		// initialize event log
		BLRE::Detours::DetourBase::eventLog = new EventLog();

		// detour UObject::ProcessEvent
		PeDetour = new Detours::ProcessEvent();
		PeDetour->Attach();
	}

	// create UObject::CallFunction detour
	if ((isServer && debugConfig.Server.LogCallFunction) || (!isServer && debugConfig.Client.LogCallFunction)) {
		// initialize event log if not already
		if(!BLRE::Detours::DetourBase::eventLog)
			BLRE::Detours::DetourBase::eventLog = new EventLog();

		// detour UObject::CallFunction
		CfDetour = new Detours::CallFunction();
		CfDetour->Attach();
	}
#endif

	// initialize detour registry
	Detours::FunctionDetour::StaticLog = this->LogFactory->Create("FunctionDetour");
	Detours = new Detours::FunctionDetourRegistry();

	// initialize user authentication interface (hijack SteamAPI_Init if run as server)
	UserAuth::InitializeSteamApi();

	// initialize console
	if (Config.Console.Enable == true) {
		Console = new UI::Console();
		if (!bConsoleAttached) {
			UI::Console::AttachOrCreate();
		}
	}

	// wait for UOnlineSubsystemPW::Init to be initialized
	while (UObject::GObjObjects()->Count < 45563 || UObject::GObjObjects()->at(45563) == nullptr || ((UFunction*)UObject::GObjObjects()->at(45563))->Func == nullptr)
		Sleep(100);
	
	// initialize user authentication on server side
	if (Utils::IsServer()) {
		auto userAuth = new UserAuth();
		userAuth->Init();
	}

	this->Patcher = new BLRE::Patcher(this);

	auto opwInitDt = Detours::FunctionDetour::Create(
		(UFunction*)UObject::GObjObjects()->at(45563),
		+[](DETOUR_FUNC_ARGS_EVENT_IMPL(UOnlineSubsystemPW, Init, opw)) {
			auto blre = BLRevive::GetInstance();
			blre->ModuleMgr = new Modules::Manager();
			blre->ModuleMgr->LoadAllModulesFromConfig();
			detour->Disable();
			detour->Continue();

			if (!Utils::IsServer())
				blre->Patcher->LoadPatchConfig();
		}
	);
	opwInitDt->Enable();

	Log->info("Initialization finished!");

	// handle console input
	if(Console != nullptr)
		Console->Run();
}

BLRE::BLRevive::BLRevive()
{

}

BLRE::BLRevive::~BLRevive()
{
#ifdef DEBUG
	if (PeDetour != nullptr && PeDetour->IsDetoured())
		PeDetour->Detach();
	if (CfDetour != nullptr && CfDetour->IsDetoured())
		CfDetour->Detach();
#endif
}

void InitializeThread()
{
	auto blrevive = BLRE::BLRevive::Create();
	blrevive->Initialize();
}

BOOL APIENTRY DllMain(HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved
)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
#ifdef DEBUG
		if (IsDebuggerPresent()) {
			BLRE::DebugUtils::InjectCustomCommandLine();
		}
#endif

		if (!std::filesystem::exists("steam_appid.txt"))
		{
			std::ofstream out("steam_appid.txt");
			out << "209870";
			out.close();
		}
		
        CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)InitializeThread, NULL, NULL, NULL);
        break;
    case DLL_THREAD_ATTACH:
		break;
    case DLL_THREAD_DETACH:
		break;
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

