#include <BLRevive/Log.h>
#include <BLRevive/Utils.h>

using namespace BLRE;

LogFactory::TSharedLogger LogFactory::Get(std::string name, LogLevel logLevel, std::string fileName)
{
	if (_Logs.find(name) == _Logs.end())
		return Create(name, logLevel, fileName);

	return _Logs[name];
}

LogFactory::TSharedLogger LogFactory::Get(std::string name)
{
	return Get(name, DefaultLogLevel, DefaultLogFile);
}

LogFactory::TSharedLogger LogFactory::Get(std::string name, LogLevel logLevel)
{
	return Get(name, logLevel, DefaultLogFile);
}

LogFactory::TSharedLogger LogFactory::Get(std::string name, std::string fileName)
{
	return Get(name, DefaultLogLevel, fileName);
}

void LogFactory::SetDefaultFile(std::string fileName)
{
	DefaultLogFile = fileName;
}

std::string LogFactory::GetDefaultFile()
{
	return DefaultLogFile;
}

void LogFactory::SetLogTarget(LoggerTarget target)
{
	_LogTarget = target;
}

LoggerTarget LogFactory::GetLogTarget()
{
	return _LogTarget;
}

LogFactory::TSharedLogger LogFactory::Create(std::string name, LogLevel level, std::string fileName)
{
	std::string logFile = fmt::format(fileName,
		fmt::arg("server", Utils::IsServer() ? "server-" : ""),
		fmt::arg("timestamp", std::to_string(StartupTime)));

	TSharedSink sink = nullptr;
	if (_Sinks.find(logFile) == _Sinks.end()) {
		if (_LogTarget == LoggerTarget::FILE) {
			std::string logFilePath(Utils::FS::BlreviveLogPath() + "\\" + logFile);
			sink = std::make_shared<spdlog::sinks::basic_file_sink_mt>(logFilePath);
			_Sinks[logFile] = sink;
		} else {
			sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
			_Sinks[logFile] = sink;
		}
	} else {
		sink = _Sinks[logFile];
	}

	auto logger = std::make_shared<structured_logger>(name, sink);
	logger->set_level((spdlog::level::level_enum)level);
	logger->flush_on(spdlog::level::trace);
	logger->set_pattern("[%X.%e.%f] [%=8l] [%=20n]  %v");

	if (_Logs.find(name) == _Logs.end())
		_Logs[name] = logger;


	return logger;
}

LogFactory::TSharedLogger LogFactory::Create(std::string name)
{
	return Create(name, DefaultLogLevel, DefaultLogFile);
}

LogFactory::TSharedLogger LogFactory::Create(std::string name, LogLevel logLevel)
{
	return Create(name, logLevel, DefaultLogFile);
}

LogFactory::TSharedLogger LogFactory::Create(std::string name, std::string fileName)
{
	return Create(name, DefaultLogLevel, fileName);
}

void LogFactory::SetDefaultLevel(LogLevel level)
{
	LogFactory::DefaultLogLevel = level;
}

LogLevel LogFactory::GetDefaultLevel()
{
	return LogFactory::DefaultLogLevel;
}