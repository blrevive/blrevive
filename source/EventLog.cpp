#include <BLRevive/EventLog.h>
#include <spdlog/fmt/fmt.h>
#include <ctime>
#include <BLRevive/Utils.h>

using namespace BLRE;

EventLog::EventLog()
{
	logFilePath = fmt::format("{}/debug-{}.csv", Utils::FS::BlreviveLogPath(), std::to_string(time(NULL)));
	logFile.open(logFilePath, std::ios::out);
}



void EventLog::log(UObject* Object, UFunction* Function, bool isEvent)
{
	auto fullObjName = std::string(Object->GetFullName());
	auto fullFuncName = std::string(Function->GetFullName()).substr(8);

	logFile << fmt::format("{},{},{},{},{}\n", std::to_string(time(NULL)), (int)isEvent, level, fullObjName, fullFuncName);
	logFile.flush();
}