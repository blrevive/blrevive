#include <BLRevive/DebugUtils.h>

using namespace BLRE::DebugUtils;

static wchar_t cmdLineW[256] = L"";
static char	cmdLineA[256] = "";

LPWSTR BLRE::DebugUtils::HkGetCommandLineW()
{
	// return our custom command line to override default
	return cmdLineW;
}

LPSTR HkGetCommandLineA()
{
	return cmdLineA;
}

void BLRE::DebugUtils::GetUserCmd()
{
	// get debug configuration
	auto debugConfig = GetDebugConfig();

	// get app path from current command line
	std::string currCmd(GetCommandLineA());
	std::string app = currCmd.substr(0, currCmd.find_last_of("\"") + 1);

	// let user chose operation mode (server/client)
	bool startAsServer = false;
	int ret = MessageBoxA(nullptr, "Start as server ?", "Chose operation mode", MB_YESNO);
	if (ret == IDYES)
		startAsServer = true;

	// set default url params for desired mode
	char buffer[256];
	if (startAsServer)
		strcpy(buffer, debugConfig.Server.URL.c_str());
	else
		strcpy(buffer, debugConfig.Client.URL.c_str());

	// let user override default params
	ret = InputBox(nullptr, "Command line args: ", "Chose", buffer, 256);

	// set custom command line
	std::string cmd = fmt::format("{} {} {}", app, startAsServer ? "server" : "", std::string(buffer));
	strcpy(cmdLineA, cmd.c_str());
	mbstowcs(cmdLineW, cmd.c_str(), cmd.length());
}

void BLRE::DebugUtils::InjectCustomCommandLine()
{
	// get user command line
	GetUserCmd();

	// detour Kernel32::GetCommandLineW to inject our custom command line
	auto handle = GetModuleHandle("KERNEL32.dll");
	auto cmdLineW = GetProcAddress(handle, "GetCommandLineW");
	auto cmdLineA = GetProcAddress(handle, "GetCommandLineA");

	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourAttach(&(PVOID&)cmdLineW, HkGetCommandLineW);
	DetourAttach(&(PVOID&)cmdLineA, HkGetCommandLineA);

	long result = DetourTransactionCommit();
	if (result != NO_ERROR)
	{
		MessageBoxA(nullptr, "Failed to detour GetCommandLineW", "Detour Failed", MB_OK);
	}
}

void* BLRE::DebugUtils::StartCompanionProcess()
{
	bool isServer = BLRE::Utils::IsServer();
	auto debugConfig = BLRE::DebugUtils::GetDebugConfig();

	// get app path from current command line
	std::string currCmd(GetCommandLineA());
	std::string app = currCmd.substr(0, currCmd.find_last_of("\"") + 1);

	bool createCompanion = false;
	char cmd[256];

	if (isServer && debugConfig.Server.AttachClient)
	{
		createCompanion = true;
		strcpy(cmd, const_cast<char*>(fmt::format("{} {}", app, debugConfig.Client.URL).c_str()));
	}
	else if (!isServer && debugConfig.Client.StartServer)
	{
		createCompanion = true;
		strcpy(cmd, const_cast<char*>(fmt::format("{} server {}", app, debugConfig.Server.URL).c_str()));
	}

	if (createCompanion)
	{
		STARTUPINFO startupInfo = {};
		PROCESS_INFORMATION processInfo;
		if (!CreateProcess(NULL, cmd, NULL, NULL, true, 0, NULL, NULL, &startupInfo, &processInfo))
		{
			MessageBox(nullptr, fmt::format("Failed to start companion process with: {}", cmd).c_str(), "Companion failed", MB_OK);
			return processInfo.hProcess;
		}
	}

	return nullptr;
}



DebugConfig BLRE::DebugUtils::GetDebugConfig()
{
	static DebugConfig debugConfig;
	static bool parsed = false;
	
	if (parsed)
		return debugConfig;

	try {
		std::string debugConfigFilename = BLRE::Utils::FS::BlreviveConfigPath() + "debug.json";
		std::ifstream debugConfigFile(debugConfigFilename);
		if (!debugConfigFile.good())
		{
			nlohmann::json debugConfig = DebugConfig();

			std::ofstream debugConfigFileOut(debugConfigFilename);
			debugConfigFileOut << debugConfig.dump(4);
			debugConfigFileOut.close();
		}
		else
		{
			nlohmann::json j = nlohmann::json::parse(debugConfigFile);
			debugConfig = j.get<DebugConfig>();
			debugConfigFile.close();
		}
	}
	catch (nlohmann::json::exception jex) {
		MessageBoxA(nullptr, (std::string("Failed to load debug config: ") + jex.what()).c_str(), "Json Exception", MB_OK);
		exit(1);
	}
	catch (std::exception ex) {
		MessageBoxA(nullptr, (std::string("Failed to load debug config: ") + ex.what()).c_str(), "Unknown Exception", MB_OK);
		exit(1);
	}

	return debugConfig;
}