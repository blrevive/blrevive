#include <BLRevive/Utils/URL.h>
#include <BLRevive/Utils.h>
#include <regex>

using namespace BLRE::Utils;

URL URL::Parse(std::string cmd)
{
    static const std::regex cmdrgx("\"?(.+\.exe){1}\"? *((server) *|(([\\d\\.]+)\\:?(\\d+)?)\\??)?(.+)*");
    static const std::regex urlrgx("(([\\w\\.]+)\\=?([^\\?]+)?)");

    URL url = {};

    std::smatch cmdMatch;
    if(!std::regex_match(cmd, cmdMatch, cmdrgx)) {
        return url;
    }

    auto srv = cmdMatch.str(3);
    auto ip = cmdMatch.str(5);
    auto port = cmdMatch.str(6);
    auto options = cmdMatch.str(7);

    if(!ip.empty()) {
        url.params["serveraddress"] = ip;
    }
    if (!port.empty()) {
        url.params["port"] = port;
    }

    if(!options.empty()) {
        bool firstParam = !srv.empty();
        for(std::smatch opt; std::regex_search(options, opt, urlrgx);) {
            auto key = opt.str(2);
            auto val = opt.str(3);

            if(firstParam && val.empty()) {
                url.params["map"] = key;
                firstParam = false;
            } else {
                String::ToLower(key);
                url.params[key] = val.empty() ? "1" : val;
            }

            options = opt.suffix();
        }
    }

    return url;
}

template<>
std::string URL::GetParam(std::string key, std::string defaultValue)
{
    String::ToLower(key);
    auto ix = params.find(key);
    if (ix == params.end())
        return defaultValue;

    return ix->second;
}

template<>
const char* URL::GetParam(std::string key, const char* defaultValue)
{
    String::ToLower(key);
    auto ix = params.find(key);
    if (ix == params.end())
        return defaultValue;

    return ix->second.c_str();
}

template<>
int URL::GetParam(std::string key, int defaultValue)
{
    return std::stoi(GetParam(key, std::to_string(defaultValue)));
}

template<>
float URL::GetParam(std::string key, float defaultValue)
{
    return std::stof(GetParam(key, std::to_string(defaultValue)));
}

template<>
long URL::GetParam(std::string key, long defaultValue)
{
    return std::stol(GetParam(key, std::to_string(defaultValue)));
}

template<>
bool URL::GetParam(std::string key, bool defaultValue)
{
    auto val = GetParam(key, std::to_string(defaultValue));
    String::ToLower(val);

    return val == "true" || val == "1";
}
