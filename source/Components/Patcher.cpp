#include <BLRevive/Components/Patcher.h>
#include <BLRevive/Utils.h>
#include <SdkHeaders.h>
#include <filesystem>
#include <httplib.h>
#include <BLRevive/Resources.h>
#include <BLRevive/resource.h>
#include <BLRevive/BLRevive.h>

using namespace BLRE;
#define CPF_Config 0x4000

void Patcher::LoadPatchConfig(std::string filePath)
{
	try {
		Log->info("loading patch files");

		nlohmann::json patchFile;
		if (!Utils::IsServer() && GetPatchFileFromServer(patchFile)) {
			Log->info("loaded patch files from server");
		}
		else if (Utils::IsServer() && filePath != "" && GetPatchFileFromDisk(filePath, patchFile)) {
			Log->info("loaded patch file from disk ({})", filePath);
		}
		else if (GetPatchFileFromResources(patchFile)) {
			Log->info("loaded patch files from resources");
		}
		else {
			throw std::exception("failed to get patch file from server and resources");
		}

		patchConfig = patchFile;
		ApplyClassPatches(patchFile);

		auto dtUnlockablesRegistered = Detours::FunctionDetour::Create("FoxGame.FoxDataStore_Unlockables.Registered",
			+[](DETOUR_FUNC_ARGS_EVENT_IMPL(UFoxDataStore_Unlockables, Registered, gri))
			{
				detour->Disable();
				detour->Continue();

				auto blre = BLRevive::GetInstance();
				blre->Patcher->ApplyGearPatches(blre->Patcher->GetPatchConfig());
			});
		dtUnlockablesRegistered->Enable();
	}
	catch (std::exception& e) {
		Log->error("failed to load patch file: {}", e.what());
	}
}

bool Patcher::GetPatchFileFromServer(nlohmann::json& patchFile)
{
	std::string server = fmt::format("http://{}:{}", blre->URL.GetParam("ServerAddress", "127.0.0.1"), blre->URL.GetParam("Port", "7777"));
	httplib::Client c(server);
	if (auto res = c.Get("/patches")) {
		if (res->status != 200) {
			Log->error("failed to pull patches from {}: status {}", server, res->status);
			return false;
		}
		try {
			patchFile = nlohmann::json::parse(res->body);
			return true;
		}
		catch (nlohmann::json::exception& jex) {
			Log->error("failed to parse server patch file: {}", jex.what());
			return false;
		}
	}
	else {
		auto err = res.error();
		Log->error("failed to pull patches from {}: {}", server, httplib::to_string(err));
		return false;
	}
	return false;
}

bool Patcher::GetPatchFileFromResources(nlohmann::json& patchFile)
{
	patchFile = Resources::Get<nlohmann::json>(RES_BLRE_PATCH_FILE);
	return true;
}

bool Patcher::GetPatchFileFromDisk(std::string path, nlohmann::json& patchFile)
{
	try {
		patchFile = Utils::ReadJsonFile<nlohmann::json>(path);
		return true;
	}
	catch (std::exception& ex) {
		Log->warn("failed to read patch file from disk ({})", path);
		return false;
	}
}

std::unordered_map<std::string, UProperty*> Patcher::GetClassConfigurableProps(UClass* cls)
{
	if (ClassConfigPropsCache.find(cls) != ClassConfigPropsCache.end())
		return ClassConfigPropsCache[cls];

	std::unordered_map<std::string, UProperty*> configProps = {};

	for (UProperty* prop = (UProperty*)cls->Children; prop != nullptr; prop = (UProperty*)prop->Next) {
		if (prop->IsA<UProperty>() && (Utils::IsServer || prop->PropertyFlags.A & CPF_Config)) {
			const char* name = prop->GetName();
			configProps[name] = prop;
		}
	}

	if (cls->SuperField && cls->SuperField != UClass::StaticClass()) {
		auto parentClsProps = GetClassConfigurableProps((UClass*)cls->SuperField);
		configProps.insert(parentClsProps.begin(), parentClsProps.end());
	}

	ClassConfigPropsCache[cls] = configProps;
	return configProps;
}

std::unordered_map<std::string, UProperty*> GetStructProps(UStruct* str)
{
	std::unordered_map<std::string, UProperty*> props = {};
	for (UField* field = str->Children; field != nullptr; field = field->Next)
		if (field->IsA<UProperty>())
			props[field->GetName()] = (UProperty*)field;

	return props;
}

bool SetPropertyInstanceValue(UProperty* prop, uintptr_t ptr, const nlohmann::json& val)
{
	uintptr_t propAddr = ptr + prop->Offset;

	if (prop->IsA<UIntProperty>()) {
		*(int*)propAddr = val.get<int>();
	}
	else if (prop->IsA<UFloatProperty>()) {
		*(float*)propAddr = val.get<float>();
	}
	else if (prop->IsA<UBoolProperty>()) {
		auto boolProp = (UBoolProperty*)prop;
		if (val.get<bool>())
			*(unsigned long*)propAddr |= boolProp->BitMask;
		else
			*(unsigned long*)propAddr &= ~boolProp->BitMask;
	}
	else if (prop->IsA<UByteProperty>()) {
		*(char*)propAddr = val.get<char>();
	}
	else if (prop->IsA<UStrProperty>()) {
		*(FString*)propAddr = FString(val.get<std::string>().c_str());
	}
	else if (prop->IsA<UNameProperty>()) {
		*(FName*)propAddr = FName(val.get<std::string>().c_str());
	}
	else if (prop->IsA<UStructProperty>()) {
		auto strProps = GetStructProps(((UStructProperty*)prop)->Struct);
		for (auto it = val.begin(); it != val.end(); ++it) {
			auto strProp = strProps.find(it.key().c_str());
			if (strProp == strProps.end())
				continue;

			SetPropertyInstanceValue(strProp->second, propAddr, it.value());
		}
	}
	else if (prop->IsA<UArrayProperty>()) {
		auto arrProp = ((UArrayProperty*)prop)->Inner;
		auto arrLen = val.size();
		*(uintptr_t*)propAddr = (uintptr_t)calloc(arrLen, arrProp->ElementSize);
		for (int i = 0; i < arrLen; ++i) {
			SetPropertyInstanceValue(arrProp, *(uintptr_t*)propAddr + (arrProp->ElementSize * i), val[i]);
		}
		*(int*)(propAddr + 0x4) = arrLen;
		*(int*)(propAddr + 0x8) = arrLen;
	}
	else if (Utils::IsServer && prop->IsA<UClassProperty>()) {
		std::string className = val.get<std::string>();
		UClass* cls = UClass::FindClass(const_cast<char*>(std::string("Class " + className).c_str()));
		UClassProperty* clsProp = (UClassProperty*)prop;

		if (cls != nullptr && (clsProp->MetaClass == nullptr || cls->ClassIsChildOf(cls, clsProp->MetaClass))) {
			*(UClass**)propAddr = cls;
		}
	}
	else {
		return false;
	}
	return true;
}

void Patcher::MapClassDefaultProperties(UClass* cls, const nlohmann::json& properties)
{
	auto clsDefault = UObject::GetDefaultInstanceOf(cls);
	auto clsProps = GetClassConfigurableProps(cls);

	if (clsDefault == nullptr) {
		Log->warn("failed to find default instance for {}", cls->GetName());
		return;
	}

	for (auto it = properties.begin(); it != properties.end(); ++it) {
		auto clsPropIt = clsProps.find(it.key());
		if (clsPropIt == clsProps.end()) {
			Log->error("failed to override {} on {}: property does not exist", it.key(), cls->GetName());
			continue;
		}

		Log->debug("overriding property {} on {}", it.key(), cls->GetName());
		if (!SetPropertyInstanceValue(clsPropIt->second, (uintptr_t)clsDefault, it.value())) {
			Log->error("failed to override property {} on {}: property is not of supported type", it.key(), cls->GetName());
		}
	}
}

void Patcher::MapGearProperties(UFoxDataProvider_GearInfo* gear, const nlohmann::json& properties)
{
	auto gearProps = GetClassConfigurableProps(gear->Class);

	for (auto it = properties.begin(); it != properties.end(); ++it) {
		auto clsPropIt = gearProps.find(it.key());
		if (clsPropIt == gearProps.end()) {
			Log->error("failed to override {} on {}: property does not exist", it.key(), gear->GetName());
			continue;
		}

		Log->debug("overriding property {} on {}", it.key(), gear->GetName());
		if (!SetPropertyInstanceValue(clsPropIt->second, (uintptr_t)gear, it.value())) {
			Log->error("failed to override property {} on {}: property is not of supported type", it.key(), gear->GetName());
		}
	}
}

void Patcher::MapModProperties(UFoxDataProvider_ModBase* mod, const nlohmann::json& properties)
{
	auto modProps = GetClassConfigurableProps(mod->Class);

	for (auto it = properties.begin(); it != properties.end(); ++it) {
		auto clsPropIt = modProps.find(it.key());
		if (clsPropIt == modProps.end()) {
			Log->error("failed to override {} on {}: property does not exist", it.key(), mod->GetName());
			continue;
		}

		Log->debug("overriding property {} on {}", it.key(), mod->GetName());
		if (!SetPropertyInstanceValue(clsPropIt->second, (uintptr_t)mod, it.value())) {
			Log->error("failed to override property {} on {}: property is not of supported type", it.key(), mod->GetName());
		}
	}
}

void Patcher::ApplyClassPatches(const nlohmann::json& patches)
{
	for (auto it = patches.begin(); it != patches.end(); ++it) {
		std::string itemName = it.key();
		UClass* cls = UClass::FindClass(const_cast<char*>(std::string("Class " + itemName).c_str()));

		if (cls != nullptr) {
			MapClassDefaultProperties(cls, it.value());
			continue;
		}

		if (!itemName.starts_with("FoxDataProvider_")) {
			Log->warn("could not find class {}", itemName);
		}
	}
}

void Patcher::ApplyGearPatches(const nlohmann::json& patches)
{
	for (auto it = patches.begin(); it != patches.end(); ++it) {
		std::string itemName = it.key();

		if (itemName.starts_with("FoxDataProvider_Gear")) {
			auto gear = (UFoxDataProvider_GearInfo*)UObject::FindObject(const_cast<char*>(itemName.c_str()));
			if (gear != nullptr) {
				MapGearProperties(gear, it.value());
				continue;
			}

			Log->warn("could not find gear {}", itemName);
		}
		else if (itemName.starts_with("FoxDataProvider_Magazine")
			|| itemName.starts_with("FoxDataProvider_Muzzle")
			|| itemName.starts_with("FoxDataProvider_Ammo")
			|| itemName.starts_with("FoxDataProvider_Hangers")
			|| itemName.starts_with("FoxDataProvider_Muzzle")
			|| itemName.starts_with("FoxDataProvider_WeaponSkin")) {
			auto mod = (UFoxDataProvider_ModBase*)UObject::FindObject(const_cast<char*>(itemName.c_str()));
			if (mod != nullptr) {
				MapModProperties(mod, it.value());
				continue;
			}

			Log->warn("could not find mod {}", itemName);
		}
		else if (itemName.starts_with("FoxDataProvider_")) {
			Log->warn("not supported class {}", itemName);
		}
	}
}

nlohmann::json& Patcher::GetPatchConfig()
{
	return patchConfig;
}