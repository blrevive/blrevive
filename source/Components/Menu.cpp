#include <BLRevive/Components/Menu.h>

using namespace BLRE::UI;

std::string BLRE::UI::trim(const std::string& str, const std::string& whitespace)
{
	const auto strBegin = str.find_first_not_of(whitespace);
	if (strBegin == std::string::npos)
		return ""; // no content

	const auto strEnd = str.find_last_not_of(whitespace);
	const auto strRange = strEnd - strBegin + 1;

	return str.substr(strBegin, strRange);
}

Menu* Menu::RootMenu()
{
	for (auto p = parent; p; p = p->parent)
		if (p->IsRoot())
			return p;
	return this;
}

bool Menu::AddCommand(std::string name, TCallback clb)
{
	// check if command with name exists
	if (commands.find(name) != commands.end())
		return false;

	commands[name] = clb;
	return true;
}

Menu* Menu::AddMenu(std::string name, Menu menu)
{
	if (menus.find(name) != menus.end())
		return nullptr;

	menu.parent = this;
	menus[name] = menu;
	return &menus[name];
}

Menu* Menu::GetMenu(std::string name)
{
	if (menus.find(name) != menus.end())
		return &menus[name];

	return nullptr;
}

Menu::TCallback* Menu::GetCommand(std::string name)
{
	if (commands.find(name) != commands.end())
		return &commands[name];

	return nullptr;
}

void Menu::Exec(std::string cmd, bool recursive)
{
	cmd = trim(cmd);
	auto clb = GetCommand(cmd);

	if (recursive && clb == nullptr)
	{
		for (auto parent = this->GetParent(); parent; parent = parent->GetParent())
		{
			clb = GetCommand(cmd);
			if (clb != nullptr)
				break;
		}
	}

	if (clb != nullptr)
		(*clb)(cmd);
}