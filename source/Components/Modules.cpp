#include <BLRevive/Components/Modules.h>
#include <BLRevive/BLRevive.h>
#include <BLRevive/Resources.h>
#include <BLRevive/resource.h>
#include <BLRevive/Config.h>
#include <BLRevive/Utils.h>

using namespace BLRE;
using namespace BLRE::Modules;

using json = nlohmann::json;
namespace fs = std::filesystem;

typedef HMODULE(__stdcall* tLoadLibrary)(LPCSTR lpLibFileName);

namespace semver {
	void from_json(const nlohmann::json& j, version& v)
	{
		v.from_string(j.get<std::string>());
	}

	void to_json(nlohmann::json& j, const version& v)
	{
		j = v.to_string();
	}
}

Modules::Manager::Manager()
	: Component("ModuleManager")
{
	try {
		auto BlreInfo = ModuleInfo(Resources::Get<nlohmann::json>(RES_BLRE_MODULE_INFO));
		BlreInfo.DllInstance = GetModuleHandleA(nullptr);
		BlreInfo.FileName = "BLRevive.dll";
		LoadedModules[BlreInfo.Name] = BlreInfo;
		Log->info("BLRevive Version: v{}", BlreInfo.Version.to_string());
	}
	catch (std::exception& ex) {
		Log->error("failed to retrieve BLRE module info:\n\t{}", ex.what());
	}
}

bool Modules::Manager::LoadModule(std::string ModuleName)
{
	static semver::version BlreVersion = LoadedModules["BLRevive"].Version;

	// check if module file was already loaded
	for (auto mod : LoadedModules) {
		if (mod.second.FileName == ModuleName) {
			return true;
		}
	}
		
	std::string ModuleFile(Utils::FS::BlreviveModulePath() + ModuleName + ".dll");

	Log->debug("loading module from {}", ModuleFile);

	// load the dll
	HINSTANCE hModule = LoadLibraryA(ModuleFile.c_str());
	if (!hModule) {
		Log->error("Failed to load module {} because of {}", ModuleName + ".dll", GetLastError());
		return false;
	}

	try {
		ModuleInfo mInfo(Resources::Get<nlohmann::json>(100, ModuleName));
		mInfo.DllInstance = hModule;
		mInfo.FileName = ModuleName;

		// get initializer function
		mInfo.Initializer = (ModuleInitializer)GetProcAddress(hModule, "InitializeModule");
		if (!mInfo.Initializer) {
			Log->error("Module {} has no initializer!", ModuleName + ".dll");
			return false;
		}

		Log->info("loaded module {}@v{} ({})", mInfo.Name, mInfo.Version.to_string(), ModuleName + ".dll");

		LoadedModules[mInfo.Name] = mInfo;
	} catch (int e) {
		Log->error("Module info failed: {}", e);
		return false;
	}

	return true;
}

bool Modules::Manager::AreModuleDependenciesMatched(ModuleInfo& module)
{
	for (auto& [modName, modVersion] : module.Requires) {
		// check if dependency was loaded
		if (LoadedModules.find(modName) == LoadedModules.end()) {
			Log->error("module {} required in {} is not loaded", modName, module.Name);
			return false;
		}

		// check if version is satisfied
		if (!semver::range::satisfies(LoadedModules[modName].Version, modVersion, semver::range::satisfies_option::include_prerelease)) {
			Log->error("module {} requires {}@{} but is {}", module.Name, modName, modVersion, LoadedModules[modName].Version.to_string());
			return false;
		}
	}

	return true;
}

bool Modules::Manager::LoadAllModulesFromConfig()
{
	Log->debug("Loading modules from config");

	auto blrevive = BLRevive::GetInstance();

	// try loading all modules from directory if no modules are configured
	// todo: replace with cli args --modules=module1,foo,bar
	if(blrevive->Config.Modules.size() == 0) {
		Log->debug(Utils::FS::BlreviveModulePath());
		try {
			fs::path modulesPath = Utils::FS::BlreviveModulePath();
			Log->debug("no modules configured, trying to load all modules from {}", modulesPath.string());

			auto modulePaths = Utils::FS::GetFilesWithExtension(modulesPath, ".dll");
			for(auto modulePath : modulePaths) {
				Log->debug("discovered module {}", modulePath.filename().string());
				blrevive->Config.Modules.insert({modulePath.stem().string(), json()});
			}
		} catch(std::exception ex) {
			Log->error(ex.what());
		}
	}

	auto mods = blrevive->Config.Modules;

	bool AllModulesLoaded = true;

	for (auto mod : mods)
		if (!LoadModule(mod.first))
			AllModulesLoaded = false;

	for (auto& [modName, modInfo] : LoadedModules) {
		// skip blrevive
		if (modName == "BLRevive")
			continue;

		// check dependencies
		if (!AreModuleDependenciesMatched(modInfo)) {
			Log->error("unmatched dependencies for {}, skipping initialization", modName);
			continue;
		}

		// initialize module
		try {
			modInfo.Instance = modInfo.Initializer(blrevive);
		}
		catch (std::exception ex) {
			Log->error("Failed to initialize module {}: {}", modName, ex.what());
		}
		catch (int ex) {
			Log->error("Failed to initialize module {}: Unknown error {}", modName, ex);
		}
	}

	return true;
}