#include <BLRevive/Detours/DetourRegistry.h>

using namespace BLRE::Detours;

FunctionDetourRegistry::FunctionDetourRegistry()
	: Component("DetourRegistry")
{}

FunctionDetourMap* FunctionDetourRegistry::GetDetourMap()
{
	return &this->DetourMap;
}

void FunctionDetourRegistry::AddDetour(uint32_t functionId, FunctionDetour* detour)
{
	if (this->DetourMap.find(functionId) == this->DetourMap.end())
	{
		this->Log->trace("attaching detour wrapper")({ {"FunctionID", functionId}});
		this->DetourMap[functionId] = {};
		this->AttachDetourWrapper(functionId);
	}

	auto functionDetours = this->DetourMap[functionId];
	if (std::find(functionDetours.begin(), functionDetours.end(), detour) != functionDetours.end())
	{
		this->Log->warn("detour is already registered")({
			{"FunctionID", functionId},
			{"Detour", fmt::format("0x{:X",(uintptr_t)detour)}
		});
		return;
	}

	this->DetourMap[functionId].push_back(detour);
	this->Log->debug("detour added")({
		{"FunctionID", functionId},
		{"Detour", fmt::format("0x{:X}", (uintptr_t)detour)}
	});
}

void FunctionDetourRegistry::RemoveDetour(uint32_t functionId, FunctionDetour* detour)
{
	if (this->DetourMap.find(functionId) == this->DetourMap.end())
	{
		this->Log->warn("failed to remove detour")({
			{"FunctionID", functionId},
			{"Detour", fmt::format("0x{:X}", (uintptr_t)detour)},
			{"Reason", "no detour for function found"}
		});
		return;
	}

	auto functionDetours = this->DetourMap[functionId];
	auto detourIt = std::find(functionDetours.begin(), functionDetours.end(), detour);

	if (detourIt == functionDetours.end())
	{

		this->Log->warn("failed to remove detour")({
			{"FunctionID", functionId},
			{"Detour", fmt::format("0x{:X}", (uintptr_t)detour)},
			{"Reason", "detour not added to function"}
		});
		return;
	}

	functionDetours.erase(detourIt);
	this->Log->debug("removed detour")({ 
		{"FunctionID", functionId},
		{"Detour", fmt::format("0x{:X}", (uintptr_t)detour)}
	});

	if (functionDetours.empty())
	{
		this->Log->trace("detaching detour wrapper")({ {"FunctionID", functionId} });
		this->DetachDetourWrapper(functionId);
	}
}

std::vector<FunctionDetour*>* FunctionDetourRegistry::GetDetoursOf(uint32_t functionId)
{
	if (this->DetourMap.find(functionId) != this->DetourMap.end())
		return &this->DetourMap[functionId];
	else
		return nullptr;
}

bool FunctionDetourRegistry::HasDetours(uint32_t functionId)
{
	return this->DetourMap.find(functionId) != this->DetourMap.end() &&
		this->DetourMap[functionId].size() > 0;
}

CUFunction* FunctionDetourRegistry::ValidateFunction(uint32_t functionId)
{
	auto function = (CUFunction*)UObject::GObjObjects()->at(functionId);
	if (function == nullptr)
	{
		this->Log->error("invalid function at {} because null", functionId);
		return nullptr;
	}

	if (function->Class != UFunction::StaticClass())
	{
		this->Log->error("invalid function at {} because class is {} but expected UFunction", functionId, function->Class->GetName());
		return nullptr;
	}

	return function;
}

void FunctionDetourRegistry::AttachDetourWrapper(uint32_t functionId)
{
	auto function = ValidateFunction(functionId);
	if (!function)
	{
		this->Log->error("failed to attach detour wrapper to function {}", functionId);
		return;
	}

	this->Backups[functionId] = {
		function->Func,
		function->FunctionFlags
	};

	function->Func = reinterpret_cast<void(UObject::*)(FFrame&, void*)>(&UDetourWrapper::Handler);
	this->Log->trace("attached detour wrapper")({
		{"Function ID", functionId},
		{"Function Name", function->GetFullName()},
		{"Flags", fmt::format("{:X}", function->FunctionFlags)}
	});

	if ((function->FunctionFlags & (DWORD)FunctionFlags::Native) == 0)
	{
		this->Log->trace("set native flag")({ {"Function ID", functionId} });
		function->FunctionFlags |= (DWORD)FunctionFlags::Native;
	}
}

void FunctionDetourRegistry::DetachDetourWrapper(uint32_t functionId)
{
	auto function = ValidateFunction(functionId);
	if (!function)
	{
		this->Log->error("failed to detach detour wrapper of function {}", functionId);
		return;
	}

	auto backup = this->Backups[functionId];
	function->Func = backup.Function;
	function->FunctionFlags = backup.Flags;

	this->Log->trace("detached detour wrapper from function {}", functionId);
}