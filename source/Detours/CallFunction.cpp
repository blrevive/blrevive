#include <BLRevive/Detours/CallFunction.h>
#include <BLRevive/BLRevive.h>
#include <BLRevive/DebugUtils.h>
#include <BLRevive/Utils.h>

using namespace BLRE::Detours;

#define CALL_FUNCTION_ADDR 0x60f10
static TCallFunction oCallFunction;
static BLRE::LogFactory::TSharedLogger CallFunctionLog = nullptr;

/**
 * Wrapper for CallFunction detour to avoid stack corruptions.
 *
 * @param This current object which function is called on
 * @param Stack current stack frame
 * @param Result pointer to result/ return param
 * @param Function function to be called
*/
void CallFunctionWrapper(UObject* This, FFrame& Stack, void* const Result, UFunction* Function)
{
    // log call function when in debug
    #if DEBUG
    static bool logCallFunction = BLRE::Utils::IsServer() ?
        BLRE::DebugUtils::GetDebugConfig().Server.LogCallFunction :
        BLRE::DebugUtils::GetDebugConfig().Client.LogCallFunction;

    if (logCallFunction)
        BLRE::Detours::DetourBase::eventLog->log(This, Function);
    #endif

    // call original CallFunction
    oCallFunction(This, Stack, Result, Function);
}

/**
 * CallFunction detour handler (the target of the detour)
 *
 * @param This current object which function is called on
 * @param _EDX unused parameter (due __thiscall to __fastcall)
 * @param Stack current stack frame
 * @param Result pointer to result/ return param
 * @param Function function to be called
*/
void __fastcall hkCallFunction(UObject* This, void* _EDX, FFrame& Stack, void* const Result, UFunction* Function)
{
    BLRE::Detours::DetourBase::eventLog->level++;

    // call wrapper
    CallFunctionWrapper(This, Stack, Result, Function);

    BLRE::Detours::DetourBase::eventLog->level--;
}

/*CallFunction::CallFunction()
    : DetourBase(&(PVOID&)oCallFunction, hkCallFunction)
{
    this->ComponentName = "CallFunctionDetour";
    oCallFunction = (TCallFunction)((uintptr_t)GetModuleHandleA(nullptr) + CALL_FUNCTION_ADDR);
}*/

CallFunction::CallFunction()
    : DetourBase(CALL_FUNCTION_ADDR, hkCallFunction, "CallFunctionDetour")
{
    oCallFunction = (TCallFunction)((uintptr_t)GetModuleHandleA(nullptr) + CALL_FUNCTION_ADDR);
    Original = &(PVOID&)oCallFunction;
}

bool CallFunction::Attach()
{
    bool result = DetourBase::Attach();

    if (result)
        Log->debug("Attached detour for CallFunction");
    else
        Log->error("Attaching detour for CallFunction failed!");

    return result;
}

bool CallFunction::Detach()
{
    bool result = DetourBase::Detach();

    if (result)
        Log->debug("Detached detour for CallFunction");
    else
        Log->error("Detaching detour for CallFunction failed!");

    return result;
}
