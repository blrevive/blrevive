#include <BLRevive/Detours/DetourBase.h>
#include <BLRevive/BLRevive.h>
#include <detours.h>

using namespace BLRE::Detours;

DetourBase::DetourBase(uintptr_t offset, PVOID detour, std::string Name)
	: Original(nullptr), Detour(detour), _IsDetoured(false), Component(Name)
{
	OrigAddr = (PVOID)offset;
	Original = &OrigAddr;
}

bool DetourBase::Attach()
{
	if (_IsDetoured)
	{
		Log->error("Failed to attach detour to {}: allready detoured!", (DWORD)Original);
		return false;
	}

	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourAttach(Original, Detour);
	const LONG result = DetourTransactionCommit();
	if (result != NO_ERROR)
	{
		Log->error("Failed to attach detour to {}: {}!", (DWORD)Original, result);
		return false;
	}

	_IsDetoured = true;
	return true;
}

bool DetourBase::Detach()
{
	if (!_IsDetoured)
	{
		Log->error("Failed to detach detour of {}: not detoured!", (DWORD)Original);
		return false;
	}

	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourDetach(Original, Detour);
	const LONG result = DetourTransactionCommit();
	if (result != NO_ERROR)
	{
		Log->error("Failed to detach detour of {}: {}", (DWORD)Original, result);
		return false;
	}

	_IsDetoured = false;
	return true;
}