#include <BLRevive/Detours/FunctionDetour.h>
#include <spdlog/pattern_formatter.h>
#include <BLRevive/BLRevive.h>

#define LogFuncId(f) f->ObjectInternalInteger;

using namespace BLRE::Detours;

void StaticHandler(UObject* obj, FFrame& Stack, void* const Result, UFunction* function)
{
	static FunctionDetourRegistry* registry = BLRE::BLRevive::GetInstance()->Detours;
	static BLRE::LogFactory::TSharedLogger log = BLRE::BLRevive::GetInstance()->LogFactory->Create("DetourWrapper");

	auto detours = registry->GetDetoursOf(function->ObjectInternalInteger);
	if (detours == nullptr)
	{
		log->error("failed to get detours of {}", function->ObjectInternalInteger);
		return;
	}

	auto dtQueue = std::queue<FunctionDetour*>(std::deque(detours->begin(), detours->end()));

	log->trace("start queue for {}: {}", function->ObjectInternalInteger, dtQueue.size());

	auto detour = dtQueue.front();
	dtQueue.pop();

	detour->Call(obj, Stack, Result, dtQueue, false);

	if (*Stack.Code == 0x16)
		Stack.Code++;

	log->trace("finished queue for {}", function->ObjectInternalInteger);
}

void UDetourWrapper::Handler(FFrame& Stack, void* const Result)
{
	UFunction* function = nullptr;
	__asm mov function, edi;

	StaticHandler(this, Stack, Result, function);
}

FunctionDetour::FunctionDetour(UFunction* Function)
	: Function(Function), detourQueue(std::queue<FunctionDetour*>()), skipFunction(false),
	Component(fmt::format("FunctionDetour", (uintptr_t)this)),
	Object(nullptr), Stack(nullptr), Result(nullptr), continued(false)
{
	std::string packageName = Function->Outer->Outer->GetName();
	std::string className = Function->Outer->GetName();
	std::string funcName = Function->GetName();

	auto functionNameId = fmt::format("{}.{}.{}({})", packageName, className, funcName, Function->ObjectInternalInteger);

	this->Log = BLRE::BLRevive::GetInstance()->LogFactory->Create("FunctionDetour");
	this->Log->set_context({ {"Function", functionNameId} });
}


FunctionDetour::~FunctionDetour()
{
	this->Disable();
}

UFunction* FunctionDetour::GetUFunctionByName(std::string FunctionName)
{
	return UObject::FindFunction(FunctionName.c_str());
}

void FunctionDetour::Call(UObject* This, FFrame& Stack, void* Result, std::queue<FunctionDetour*> queue, bool skipFunction)
{
	this->detourQueue = queue;
	this->skipFunction = skipFunction;
	this->continued = false;

	this->Object = This;
	this->Stack = &Stack;
	this->Result = Result;
}

void FunctionDetour::Continue(bool forceSkipFunction)
{
	if (detourQueue.empty())
	{
		if (this->skipFunction || forceSkipFunction)
		{
			this->Stack->SkipFunction();
		}
		else 
		{
			auto backup = BLRE::BLRevive::GetInstance()->Detours->Backups[Function->ObjectInternalInteger];
			Function->FunctionFlags = backup.Flags;

			auto func = (CUFunction*)Function;
			func->Func = backup.Function;

			if (Function->FunctionFlags & (DWORD)FunctionFlags::Native)
				this->Object->CallFunction(*Stack, Result, Function);
			/*else if (Function->FunctionFlags & (DWORD)FunctionFlags::Event)
				this->Object->ProcessEvent((UFunction*)Function, Stack->Locals);*/
			else if (Function->ObjectInternalInteger == Stack->Node->ObjectInternalInteger)
				this->Object->ProcessInternal(*Stack, Result);
			else
				this->Object->CallFunction(*Stack, Result, Function);

			if ((Function->FunctionFlags & (DWORD)FunctionFlags::Native) == 0)
				Function->FunctionFlags |= (DWORD)FunctionFlags::Native;

			func->Func = reinterpret_cast<void(UObject::*)(FFrame&, void*)>(&UDetourWrapper::Handler);
		}
	}
	else
	{
		auto next = detourQueue.front();
		detourQueue.pop();
		next->Call(Object, *Stack, Result, detourQueue, skipFunction || forceSkipFunction);
	}

	this->Object = nullptr;
	this->Stack = {};
	this->Result = nullptr;
	this->continued = true;
}

void FunctionDetour::Skip()
{
	this->Continue(true);
}

void FunctionDetour::Enable()
{
	BLRE::BLRevive::GetInstance()->Detours->AddDetour(this->Function->ObjectInternalInteger, this);
}

void FunctionDetour::Disable()
{
	BLRE::BLRevive::GetInstance()->Detours->RemoveDetour(this->Function->ObjectInternalInteger, this);
}