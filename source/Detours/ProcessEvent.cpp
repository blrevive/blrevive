#include <BLRevive/Detours/ProcessEvent.h>
#include <BLRevive/BLRevive.h>
#include <BLRevive/DebugUtils.h>
using namespace BLRE::Detours;

#define PROCESS_EVENT_ADDR 0x61530
static TProcessEvent oProcessEvent;
static BLRE::LogFactory::TSharedLogger ProcessEventLog = nullptr;

struct FEventArgs
{
    UObject* This;
    UFunction* Function;
    void* Params;
    void* Result;
};

FEventArgs DecryptEventArgs(UObject* This, UFunction* EnFunction, void* EnParams, void* Result)
{
    // get encryption keys from memory
    static const LPENCRYPTIONBASE		pECBase = (LPENCRYPTIONBASE)((char*)GetModuleHandleA(nullptr) + pPEEncryptionBase);
    static const LPENCRYPTIONINDEX		pECIndex = (LPENCRYPTIONINDEX)((char*)GetModuleHandleA(nullptr) + pPEEncryptionIndex);
    DWORD XorUFunction = pECBase->pUFunctionBase[pECIndex->UFunctionIndex];
    DWORD XorPParams = pECBase->pPParamsBase[pECIndex->PParamsIndex];

    // decrypt function and paramters pointer
    UFunction* Function = (UFunction*)(XorUFunction ^ (DWORD)EnFunction);
    void* Params = nullptr;
    if (EnParams != nullptr)
        Params = (void*)(XorPParams ^ (DWORD)EnParams);

    return FEventArgs
    {
        This,
        Function,
        Params,
        Result
    };

}

void PreProcessEvent(FEventArgs& args)
{

    #if DEBUG
    static bool logProcessEvent = BLRE::Utils::IsServer() ?
        BLRE::DebugUtils::GetDebugConfig().Server.LogProcessEvent :
        BLRE::DebugUtils::GetDebugConfig().Client.LogProcessEvent;

    if (logProcessEvent)
        if(args.This && args.Function)
            BLRE::Detours::DetourBase::eventLog->log(args.This, args.Function, true);
    #endif
}

void PostProcessEvent(FEventArgs& args)
{
}

/**
 * ProcessEvent detour handler (the target of ProcessEvent detour)
 *
 * @param This current object event is processed on
 * @param _EDX unused parameter (due to __thiscall to __fastcall)
 * @param EnFunction function pointer (xor-encrypted)
 * @param EnParams parameters pointer (xor-encrypted)
*/
void __fastcall hkProcessevent(UObject* This, void* _EDX, UFunction* EnFunction, void* const EnParams, void* Result)
{
    FEventArgs args = DecryptEventArgs(This, EnFunction, EnParams, Result);

    BLRE::Detours::DetourBase::eventLog->level++;
    PreProcessEvent(args);

    oProcessEvent(This, EnFunction, EnParams, Result);

    PostProcessEvent(args);
    BLRE::Detours::DetourBase::eventLog->level--;

}

/*ProcessEvent::ProcessEvent()
    : DetourBase(&(PVOID&)oProcessEvent, hkProcessevent)
{
    DetourBase(&(PVOID&)oProcessEvent, hkProcessevent);
    this->ComponentName = "ProcessEventDetour";
    oProcessEvent = (TProcessEvent)
}*/
ProcessEvent::ProcessEvent()
    : DetourBase(PROCESS_EVENT_ADDR, hkProcessevent, "ProcessEventDetour")
{
    oProcessEvent = (TProcessEvent)((uintptr_t)GetModuleHandleA(nullptr) + PROCESS_EVENT_ADDR);
    Original = &(PVOID&)oProcessEvent;
}

bool ProcessEvent::Attach()
{
    bool result = DetourBase::Attach();

    if (result)
        Log->debug("Attached detour for ProcessEvent");
    else
        Log->error("Attaching detour for ProcessEvent failed!");

    return result;
}

bool ProcessEvent::Detach()
{
    bool result = DetourBase::Detach();

    if (result)
        Log->debug("Detached detour for ProcessEvent");
    else
        Log->error("Detaching detour for ProcessEvent failed!");

    return result;
}