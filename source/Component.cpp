#include <BLRevive/Component.h>
#include <BLRevive/BLRevive.h>

using namespace BLRE;

Component::Component(std::string name)
{
	ComponentName = name;
	ComponentLogFile = BLRevive::GetInstance()->LogFactory->GetDefaultFile();

	Log = BLRevive::GetInstance()->LogFactory->Get(name);
}

Component::Component(std::string name, std::string logFile)
{
	ComponentName = name;
	ComponentLogFile = logFile;

	Log = BLRevive::GetInstance()->LogFactory->Get(name, logFile);
}