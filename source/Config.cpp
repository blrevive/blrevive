#include <BLRevive/Config.h>
#include <BLRevive/BLRevive.h>
#include <BLRevive/Utils.h>
#include <fstream>
#include <filesystem>

namespace fs = std::filesystem;
using namespace BLRE;

void Config::Save(std::string path)
{
    Utils::WriteJsonFile(path, *this);
}

Config Config::Load(std::string path, bool createDefault)
{
    if (!std::filesystem::exists(path)) {
        Config defaultConfig = {};
        Utils::WriteJsonFile(path, defaultConfig);
        return defaultConfig;
    }

    return Utils::ReadJsonFile<Config>(path);
}