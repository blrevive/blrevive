#include <BLRevive/Config.h>

namespace BLRE::Examples
{
    // represantation of the config section defined in ExampleConfig.json
    struct ExampleConfig : BLRE::ConfigSection
    {
        // name of the section (= key of the json object)
        inline static std::string SectionName = "Example";

        // configurations
        bool IsDefault = false;
        std::string SomeString = "Foo";

        // in case the variable does not exist in the configuration file, the default value is used
        bool NonExistantKey = true;
    }

    // let nlohmann::json know how to handle the section (optional but useful if you want custom logic)
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(ExampleConfig, IsDefault, SomeStringConfig, NonExistanKey);
}

#include <BLRevive/BLRevive.h>
void HandleExampleConfig()
{
    using namespace BLRE::Example;

    // parse current config file passed by URL parameter
    auto config = BLRE::BLRevive::GetInstance()->Config;

    // parse another config file by filename
    config = BLRE::Config::Load("ExampleConfig");

    // read the section using the struct defined above
    ExampleConfig* exampleSection = config->Get<ExampleConfig>();
    MainLog->debug("SomeString: {}", exampleSection->SomeString);

    // overwrite variables in the section
    exampleSection->SomeString = "FooBar";
    config->Update(exampleSection);

    // config sections can also read using nlohmann::json
    //  however declaring a struct is recommended
    nlohmann::json exampleSectionJson = config->Get("Example");
    MainLog->debug("SomeString: {}", exampleSectionJson["SomeString"]);
}