Wrapper for DINPUT8.dll which loads BLRevive.dll at runtime and applies necessary patches.
This approach allows using an unpatched game file.