#pragma once
#include <string>
#include <windows.h>
#include <BLRevive/BLRevive.h>
#include <nlohmann/json.hpp>

/**
* Get access to embbed resources defined inside {Project}.rc
*/
namespace BLRE::Resources
{
	struct Parameters {
		std::size_t size = 0;
		void* ptr = nullptr;
		HRSRC res = 0;
	};

	template<typename T = Parameters>
	inline static T Get(int id, std::string moduleName = PROJECT_NAME, const std::string& cls = "TEXT")
	{
		T data;
		HMODULE hModule = GetModuleHandleA((moduleName + ".dll").c_str());
		HRSRC hResource = FindResourceA(hModule, MAKEINTRESOURCEA(id), cls.c_str());
		HGLOBAL hMemory = LoadResource(hModule, hResource);
		if (!hResource || !hMemory) {
			MainLog->error("Can't find resource {}", id);
			return data;
		}

		data.res = hResource;
		data.size = SizeofResource(hModule, hResource);
		data.ptr = LockResource(hMemory);
		return data;
	};

	template<>
	inline static std::string Get(int id, std::string moduleName, const std::string& cls)
	{
		auto data = Get<Parameters>(id, moduleName, cls);
		std::string dst(reinterpret_cast<char*>(data.ptr), data.size);
		FreeResource(data.res);
		return dst;
	};

	template<>
	inline static nlohmann::json Get(int id, std::string moduleName, const std::string& cls)
	{
		auto str = Get<std::string>(id, moduleName, cls);
		nlohmann::json j = nlohmann::json::parse(str);
		return j;
	};
}