#pragma once

namespace BLRE::Detours
{
	/**
		 * @brief A function detour to a non-owning member function
		 * @tparam TDetourClass class which owns the detour target
		 * @tparam TUClass class which owns the detour function
		 * @tparam TReturn return type of detour function
		 * @tparam TDetourArgs argument of detour function (FFrame or param struct)
		*/
	template<typename TDetourClass, typename TUClass, typename TReturn, typename TDetourArgs>
	class FunctionDetourCls : public FunctionDetour
	{
	public:
		// type definition of detour target
		using TDetour = TReturn(TDetourClass::*)(FunctionDetour*, TUClass*, TDetourArgs);
		// pointer to detour target member function
		TDetour Detour;
		// pointer to owning class
		TDetourClass* DetourInstance;

		FunctionDetourCls(UFunction* Function, TDetourClass* Instance, TDetour Detour)
			: Detour(Detour), DetourInstance(Instance), FunctionDetour(Function)
		{};

		virtual void Call(UObject* This, FFrame& Stack, void* Result, std::queue<FunctionDetour*> queue, bool skipFunction = false) override
		{
			FunctionDetour::Call(This, Stack, Result, queue, skipFunction);
			Log->trace("invoking detour")({
				{"Target", "Class"},
				{"Stack", (uintptr_t)&Stack},
				{"Result", (uintptr_t)Result},
				{"Queue Count", queue.size()},
				{"Skip Function", skipFunction}
				});

			auto result = reinterpret_cast<TReturn*>(Result);
			auto ucls = reinterpret_cast<TUClass*>(This);

			if constexpr (std::is_same_v<TReturn, void>)
				if constexpr (std::is_same_v<TDetourArgs, FFrame*>)
					(DetourInstance->*Detour)(this, ucls, Stack);
				else
					(DetourInstance->*Detour)(this, ucls, reinterpret_cast<TDetourArgs>(Stack.Locals));
			else
				if constexpr (std::is_same_v<TDetourArgs, FFrame*>)
					*result = (DetourInstance->*Detour)(this, ucls, Stack);
				else
					*result = (DetourInstance->*Detour)(this, ucls, reinterpret_cast<TDetourArgs>(Stack.Locals));

			this->ContidtionalContinue();
		}
	};
}