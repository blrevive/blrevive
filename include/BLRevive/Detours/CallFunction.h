#pragma once
#include <BLRevive/Detours/DetourBase.h>
#include <SdkHeaders.h>

namespace BLRE::Detours
{
	typedef void(__thiscall* TCallFunction)(UObject*, FFrame&, void* const, UFunction*);

	/**
	 * Helper class for detouring UObject::CallFunction.
	*/
	class CallFunction : public DetourBase
	{
	public:
		CallFunction();

		/**
		 * Detour CallFunction.
		 * 
		 * @return is detoured
		*/
		virtual bool Attach() override;

		/**
		 * Remove CallFunction detour.
		 * 
		 * @return detour removed
		*/
		virtual bool Detach() override;
	};
}