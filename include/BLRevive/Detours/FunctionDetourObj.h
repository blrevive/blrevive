#pragma once
#include <BLRevive/Detours/FunctionDetour.h>


namespace BLRE::Detours
{
	/**
		 * @brief A function detour to an owning member function
		 * @tparam TUClass class which owns the detour function
		 * @tparam TReturn return type of detour target
		 * @tparam TDetourArgs arguments of detour target (FFrame or param struct)
		*/
	template<typename TUClass, typename TReturn, typename TDetourArgs>
	class FunctionDetourObject : public FunctionDetour
	{
	public:
		// type definition of detour target
		using TDetour = TReturn(TUClass::*)(FunctionDetour*, TDetourArgs);
		// pointer to detour member function
		TDetour Detour;

		FunctionDetourObject(UFunction* Function, TDetour Detour)
			: Detour(Detour), FunctionDetour(Function)
		{}

		virtual void Call(UObject* Object, FFrame& Stack, void* Result, std::queue<FunctionDetour*> queue, bool skipFunction = false) override
		{
			FunctionDetour::Call(Object, Stack, Result, queue, skipFunction);
			Log->trace("invoking detour")({
				{"Target", "UObject"},
				{"Stack", (uintptr_t)&Stack},
				{"Result", (uintptr_t)Result},
				{"Queue Count", queue.size()},
				{"Skip Function", skipFunction}
				});

			auto cls = reinterpret_cast<TUClass*>(Object);

			if constexpr (std::is_same_v<TReturn, void>)
				if constexpr (std::is_same_v<TDetourArgs, FFrame*>)
					(cls->*Detour)(this, Stack);
				else
					(cls->*Detour)(this, reinterpret_cast<TDetourArgs>(Stack.Locals));
			else
				if constexpr (std::is_same_v<TDetourArgs, FFrame*>)
					*(TReturn*)Result = (cls->*Detour)(this, Stack);
				else
					*(TReturn*)Result = (cls->*Detour)(this, reinterpret_cast<TDetourArgs>(Stack.Locals));

			this->ContidtionalContinue();
		}
	};
}