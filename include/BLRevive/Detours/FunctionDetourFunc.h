#pragma once
#include <BLRevive/Detours/FunctionDetour.h>

namespace BLRE::Detours
{
	/**
		 * @brief A function detour to a lose function (not bound to a class)
		 * @tparam TTargetClass class which owns the detour function
		 * @tparam TDetourRet return type of the detour function
		 * @tparam TDetourArg argument of the detour function (FFrame or param struct)
		*/
	template<typename TTargetClass, typename TDetourRet, typename TDetourArg>
	class FunctionDetourFunc : public FunctionDetour
	{
	public:
		// type definition of detour target
		using TDetour = TDetourRet(*)(FunctionDetour*, TTargetClass*, TDetourArg);
		// pointer to detour target function
		TDetour Detour;

		FunctionDetourFunc(UFunction* Function, TDetour Detour)
			: Detour(Detour), FunctionDetour(Function)
		{};

		virtual void Call(UObject* This, FFrame& Stack, void* Result, std::queue<FunctionDetour*> queue, bool skipFunction = false) override
		{
			FunctionDetour::Call(This, Stack, Result, queue, skipFunction);
			Log->trace("invoking detour")({
				{"Target", "Function"},
				{"Stack", (uintptr_t)&Stack},
				{"Result", (uintptr_t)Result},
				{"Queue Count", queue.size()},
				{"Skip Function", skipFunction}
				});

			auto cls = reinterpret_cast<TTargetClass*>(This);
			auto result = reinterpret_cast<TDetourRet*>(Result);

			if constexpr (std::is_same_v<TDetourRet, void>)
				if constexpr (std::is_same_v<TDetourArg, FFrame*>)
					Detour(this, cls, Stack);
				else
					Detour(this, cls, reinterpret_cast<TDetourArg>(Stack.Locals));
			else
				if constexpr (std::is_same_v<TDetourArg, FFrame*>)
					*result = Detour(this, cls, Stack);
				else
					*result = Detour(this, cls, reinterpret_cast<TDetourArg>(Stack.Locals));

			this->ContidtionalContinue();
		}
	};
}