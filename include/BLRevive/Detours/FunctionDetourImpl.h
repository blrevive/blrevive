#pragma once
#include <BLRevive/Detours/FunctionDetour.h>
#include <BLRevive/Detours/FunctionDetourFunc.h>
#include <BLRevive/Detours/FunctionDetourCls.h>
#include <BLRevive/Detours/FunctionDetourObj.h>
#include <BLRevive/Utils.h>

namespace BLRE::Detours
{
	template<typename TDetourClass, typename TTargetClass, typename TReturn, typename TDetourArg>
	FunctionDetour* FunctionDetour::Create(UFunction* Function, TDetourClass* DetourClass, TReturn(TDetourClass::* Detour)(FunctionDetour*, TTargetClass*, TDetourArg))
	{
		auto dt = new FunctionDetourCls(Function, DetourClass, Detour);
		return dt;
	}

	template<typename TDetourClass, typename TTargetClass, typename TReturn, typename TDetourArg>
	FunctionDetour* FunctionDetour::Create(std::string FunctionName, TDetourClass* DetourClass, TReturn(TDetourClass::* Detour)(FunctionDetour*, TTargetClass*, TDetourArg))
	{
		auto function = GetUFunctionByName(FunctionName);

		if (!function)
		{
			StaticLog->error("Failed to detour function {}: not such function", FunctionName);
			return nullptr;
		}

		return FunctionDetour::Create(function, DetourClass, Detour);
	}

	template<typename TDetourClass, typename TReturn, typename TDetourArg>
	FunctionDetour* FunctionDetour::Create(UFunction* Function, TReturn(TDetourClass::* Detour)(FunctionDetour*, TDetourArg))
	{
		auto dt = new FunctionDetourObject(Function, Detour);
		return dt;
	}

	template<typename TDetourClass, typename TReturn, typename TDetourArg>
	FunctionDetour* FunctionDetour::Create(std::string FunctionName, TReturn(TDetourClass::* Detour)(FunctionDetour*, TDetourArg))
	{
		auto function = GetUFunctionByName(FunctionName);

		if (!function)
		{
			StaticLog->error("Failed to detour function {}: no such function", FunctionName);
			return nullptr;
		}

		return FunctionDetour::Create(function, Detour);
	}

	template<typename R, typename ...Args>
	FunctionDetour* FunctionDetour::Create(UFunction* Function, R(*Detour)(FunctionDetour*, Args...))
	{
		auto dt = new FunctionDetourFunc(Function, Detour);
		return dt;
	}

	template<typename R, typename ...Args>
	FunctionDetour* FunctionDetour::Create(std::string FunctionName, R(*Detour)(FunctionDetour*, Args...))
	{
		auto function = GetUFunctionByName(FunctionName);

		if (!function)
		{
			StaticLog->error("Failed to detour function {}: not such function", FunctionName);
			return nullptr;
		}

		return FunctionDetour::Create(function, Detour);
	}


	/**
	 * @brief UFunction class with UObject bound function pointer
	*/
	class CUFunction : public UStruct
	{
	public:
		uint32_t		FunctionFlags;
		uint16_t		iNative;
		uint16_t		RepOffset;
		FName			FriendlyName;
		uint8_t			Unknown0;
		uint8_t			NumParams;
		uint16_t		ParamSize;
		uint32_t		ReturnValueOffset;
		UObject* Unknown1;
		void(UObject::* Func)(struct FFrame& Stack, void* const pRes);
	};

	struct FunctionDetourBackup
	{
		void(UObject::* Function)(FFrame&, void* const);
		uint32_t Flags;
	};

	/**
	 * @brief
	*/
	class UDetourWrapper : public UObject
	{
	public:
		void Handler(FFrame& Stack, void* const Result);
	};

#define DETOUR_FUNC_ARGS(cls, fn) BLRE::Detours::FunctionDetour*, ::cls*, ::cls ## _exec ## fn ## _Parms*
#define DETOUR_FUNC_ARGS_IMPL(...) VA_MACRO(_DETOUR_FUNC_ARGS_IMPL_, __VA_ARGS__)
#define _DETOUR_FUNC_ARGS_IMPL_3(cls, fn, objName) BLRE::Detours::FunctionDetour* detour, ::cls* objName, ::cls ## _exec ## fn ## _Parms* params
#define _DETOUR_FUNC_ARGS_IMPL_2(cls, fn) _DETOUR_FUNC_ARGS_IMPL_3(cls, fn, object)


#define DETOUR_FUNC_ARGS_EVENT(cls, fn) BLRE::Detours::FunctionDetour*, ::cls*, ::cls ## _event ## fn ## _Parms*
#define DETOUR_FUNC_ARGS_EVENT_IMPL(...) VA_MACRO(_DETOUR_FUNC_ARGS_EVENT_IMPL_, __VA_ARGS__)
#define _DETOUR_FUNC_ARGS_EVENT_IMPL_3(cls, fn, objName) BLRE::Detours::FunctionDetour* detour, ::cls* objName, ::cls ## _event ## fn ## _Parms* params
#define _DETOUR_FUNC_ARGS_EVENT_IMPL_2(cls, fn) _DETOUR_FUNC_ARGS_EVENT_IMPL_3(cls, fn, object)

#define DETOUR_FUNC_ARGS_OBJ(cls, fn) BLRE::Detours::FunctionDetour* detour, ::cls ## _exec ## fn ## _Parms* params
#define DETOUR_FUNC_ARGS_OBJ_EVENT(cls, fn) BLRE::Detours::FunctionDetour* detour, ::cls ## _event ## fn ## _Parms* params


	/**
	* @brief create signature for lose function detour
	*
	* @param cb name of the callback
	* @param ucls owning UClass of the function to detour
	* @param fn function to detour
	*/
#define DETOUR_CB_FUNC(cb, ucls, fn) cb(DETOUR_FUNC_ARGS(ucls, fn))
#define DETOUR_CB_FUNC_IMPL(cb, ...) cb(DETOUR_FUNC_ARGS_IMPL(__VA_ARGS__))

	/**
	* @brief create signature for lose function detour which target is an event
	*
	* @param cb name of the callback
	* @param ucls owning UClass of the function to detour
	* @param fn function to detour
	*/
#define DETOUR_CB_FUNC_EVENT(cb, ucls, fn) cb(DETOUR_FUNC_ARGS_EVENT(ucls, fn))
#define DETOUR_CB_FUNC_EVENT_IMPL(cb, ...) cb(DETOUR_FUNC_ARGS_EVENT_IMPL(__VA_ARGS__))

	/**
	* @brief create signature for class function detour (for definition)
	*
	* @param cb name of the callback
	* @param ucls owning UClass of the function to detour
	* @param fn function to detour
	*/
#define DETOUR_CB_CLS(cb, ucls, fn) DETOUR_CB_FUNC(cb, ucls, fn)

	/**
	* @brief create signature for class function detour (for implementation)
	*
	* @param clscb namespaced callback function (DetourClass::DetourFunc)
	* @param ucls owning UClass of the function to detour
	* @param fn function to detour
	*/
#define DETOUR_CB_CLS_IMPL(clscb, ...) clscb(DETOUR_FUNC_ARGS_IMPL(__VA_ARGS__))


	/**
	* @brief create signature for class function detour which target is an event (for definition)
	*
	* @param cb name of the callback
	* @param ucls owning UClass of the function to detour
	* @param fn function to detour
	*/
#define DETOUR_CB_CLS_EVENT(cb, ucls, fn) DETOUR_CB_FUNC_EVENT(cb, ucls, fn)


	/**
	* @brief create signature for class function detour which target is an event (for implementation)
	*
	* @param clscb namespaced callback function (DetourClass::DetourFunc)
	* @param ucls owning UClass of the function to detour
	* @param fn function to detour
	*/
#define DETOUR_CB_CLS_EVENT_IMPL(clscb, ...) clscb(DETOUR_FUNC_ARGS_EVENT_IMPL(__VA_ARGS__))


	/**
	* @brief create signature for object function detour (for definition)
	*
	* @param cb name of the callback
	* @param cls owning UClass of the function to detour
	* @param fn function to detour
	*/
#define DETOUR_OBJ_CB(cb, cls, fn) cb(DETOUR_FUNC_ARGS_OBJ(cls, fn))

	/**
	* @brief create signature for object function detour which target is an event (for definition)
	*
	* @param cb name of the callback
	* @param cls owning UClass of the function to detour
	* @param fn function to detour
	*/
#define DETOUR_OBJ_EVENT_CB(cb, cls, fn) cb(DETOUR_FUNC_ARGS_OBJ_EVENT(cls, fn))

	/**
	* @brief create signature for object function detour (for implementation)
	*
	* @param objcb namespaced callback function (ObjectClass::DetourFunction)
	* @param cls owning UClass of the function to detour
	* @param fn function to detour
	*/
#define DETOUR_OBJ_CB_IMPL(objcb, cls, fn)  objcb(DETOUR_FUNC_ARGS_OBJ(cls, fn))

	/**
	* @brief create signature for object function detour which target is an event (for implementation)
	*
	* @param objcb namespaced callback function (ObjectClass::DetourFunction)
	* @param cls owning UClass of the function to detour
	* @param fn function to detour
	*/
#define DETOUR_OBJ_EVENT_CB_IMPL(objcb, cls, fn) objcb(DETOUR_FUNC_ARGS_OBJ_EVENT(cls, fn))
}