#pragma once
#include <BLRevive/Component.h>
#include <BLRevive/Detours/FunctionDetour.h>

namespace BLRE::Detours
{

	using FunctionDetourMap = std::unordered_map<uint32_t, std::vector<FunctionDetour*>>;
	class FunctionDetourRegistry : public Component
	{
	public:
		FunctionDetourRegistry();

		FunctionDetourMap* GetDetourMap();
		std::vector<FunctionDetour*>* GetDetoursOf(uint32_t functionId);
		bool HasDetours(uint32_t functionId);

		void AddDetour(uint32_t functionId, FunctionDetour* detour);
		void RemoveDetour(uint32_t functionId, FunctionDetour* detour);

		std::unordered_map<uint32_t, FunctionDetourBackup> Backups = {};
	private:
		FunctionDetourMap DetourMap = {};

		void AttachDetourWrapper(uint32_t functionId);
		void DetachDetourWrapper(uint32_t functionId);
		CUFunction* ValidateFunction(uint32_t functionId);
	};
}
