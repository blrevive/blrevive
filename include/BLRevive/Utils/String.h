#pragma once
#include <string>
#include <sstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <cctype>

namespace BLRE::Utils::String
{
    static std::vector<std::string> Split(const std::string& s, char delim) {
        std::vector<std::string> result;
        std::stringstream ss(s);
        std::string item;

        while (getline(ss, item, delim)) {
            if (item.size() == 0)
                continue;
            result.push_back(item);
        }

        return result;
    }

    static void ToLower(std::string& str)
    {
        std::transform(str.begin(), str.end(), str.begin(),
            [](unsigned char c) { return std::tolower(c); });
    }
}