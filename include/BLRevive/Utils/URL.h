#pragma once
#include <BLRevive/Utils/String.h>

#include <unordered_map>
#include <string>

template<typename E>
concept EnumType = std::is_enum_v<E>;

namespace BLRE
{
    namespace Utils { struct URL; };

    template<typename T>
    void map_url_config(T& conf, BLRE::Utils::URL url);
};

namespace BLRE::Utils
{
    struct URL
    {
    public:
        static URL Parse(std::string cmd);

        template<typename T>
        T GetParam(std::string key, T defaultValue = T());

        template<EnumType Enum>
        Enum GetParam(std::string key, Enum defaultValue)
        {
            return (Enum)GetParam(key, (std::underlying_type_t<Enum>)defaultValue);
        }
    private:
        std::unordered_map<std::string, std::string> params = {};
    };
}