#pragma once
#include <exception>
#include <string>
#include <spdlog/fmt/fmt.h>

enum exit_codes
{
	CONFIG_FILE_ERROR = 10000,
	CONFIG_FILE_NOT_FOUND,
	CONSOLE_INPUT_WITH_CONSOLE_LOG
};

struct file_not_found : std::exception
{
	file_not_found(std::string path)
		: path(path), std::exception(fmt::format("file {} not found", path).c_str())
	{}
private:
	std::string path;
};

struct file_no_access : std::exception
{
	file_no_access(std::string path)
		: path(path), std::exception(fmt::format("file {} could not be accessed", path).c_str())

	{}
private:
	std::string path;
};