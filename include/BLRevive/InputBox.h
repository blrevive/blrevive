#pragma once
#include <windows.h>
//---------------------------------------------------------------------------
LPWORD          DWORD_ALIGN(LPWORD);
LPWORD          NON_DWORD_ALIGN(LPWORD);
LPWORD          InitDialog(LPVOID, LPCTSTR, DWORD, WORD, LPCTSTR, WORD, short, short, short, short);
LPWORD          CreateDlgControl(LPWORD, WORD, WORD, LPCTSTR, DWORD, short, short, short, short);
int             InputBox(HWND, LPCTSTR, LPCTSTR, LPTSTR, INT);
BOOL  CALLBACK  InputBoxDlgProc(HWND hwnd, UINT uiMsg, WPARAM wParam, LPARAM lParam);