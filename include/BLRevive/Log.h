#pragma once
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <map>
#include <ctime>
#include <nlohmann/json.hpp>
#include <tuple>

#define DEFAULT_LOG_FILENAME "BLRevive"
#ifdef DEBUG
#define DEFAULT_LOG_LEVEL LogLevel::trace
#else
#define DEFAULT_LOG_LEVEL LogLevel::debug
#endif

static time_t StartupTime = time(nullptr);

namespace BLRE
{
	enum class LogLevel
	{
		none = -1,
		trace = SPDLOG_LEVEL_TRACE,
		debug = SPDLOG_LEVEL_DEBUG,
		info = SPDLOG_LEVEL_INFO,
		warn = SPDLOG_LEVEL_WARN,
		err = SPDLOG_LEVEL_ERROR,
		critical = SPDLOG_LEVEL_CRITICAL,
		off = SPDLOG_LEVEL_OFF
	};

	NLOHMANN_JSON_SERIALIZE_ENUM(LogLevel, {
		{LogLevel::trace, "trace"},
		{LogLevel::debug, "debug"},
		{LogLevel::info, "info"},
		{LogLevel::warn, "warn"},
		{LogLevel::err, "error"},
		{LogLevel::critical, "critical"}
		});

	enum class LoggerTarget
	{
		CONSOLE,
		FILE,
	};

	NLOHMANN_JSON_SERIALIZE_ENUM(LoggerTarget, {
		{LoggerTarget::CONSOLE, "console"},
		{LoggerTarget::FILE, "file"}
	});

	struct log_data
	{
		std::string val;
		const char* key;

		template<typename T>
			requires(std::constructible_from<std::string, T>)
		log_data(const char* k, T v) : key(k), val(v) {}

		template<typename T>
			requires(!std::constructible_from<std::string, T>)
		log_data(const char* k, T v) : key(k), val(std::to_string(v)) {}

		template<>
		log_data(const char* k, std::filesystem::path p) : key(k), val(p.string()) {}

		std::string format(const char* pattern)
		{
			return fmt::format(pattern, key, val);
		}
	};

	class structured_logger;

	template<typename ...Args>
	struct logger_executor
	{
		structured_logger* logger;
		std::string msg;
		spdlog::level::level_enum level;

		logger_executor(structured_logger* logger, spdlog::level::level_enum lvl, std::string fmt, Args... args)
		{
			this->logger = logger;
			this->msg = fmt::format(fmt, args...);
			this->level = lvl;
		}

		~logger_executor()
		{
			logger->log(level, msg);
		}

		logger_executor& operator()(std::vector<log_data> data)
		{
			msg += logger->format_data(data);
			return *this;
		}
	};

	class structured_logger
	{
	public:
		structured_logger(std::string name, spdlog::sink_ptr sink)
			: _logger(std::make_unique<spdlog::logger>(std::move(name), sink)), _context_str("")
		{}

	public:
		template<typename ...Args>
		logger_executor<Args...> trace(std::string fmt, Args... args) {
				return logger_executor(this, spdlog::level::trace, fmt, args...);
		}

		template<typename ...Args>
		logger_executor<Args...> debug(std::string fmt, Args... args) {
			return logger_executor(this, spdlog::level::debug, fmt, args...);
		}

		template<typename ...Args>
		logger_executor<Args...> info(std::string fmt, Args... args) {
			return logger_executor(this, spdlog::level::info, fmt, args...);
		}

		template<typename ...Args>
		logger_executor<Args...> warn(std::string fmt, Args... args) {
			return logger_executor(this, spdlog::level::warn, fmt, args...);
		}

		template<typename ...Args>
		logger_executor<Args...> error(std::string fmt, Args... args) {
			return logger_executor(this, spdlog::level::err, fmt, args...);
		}

		template<typename ...Args>
		logger_executor<Args...> critical(std::string fmt, Args... args) {
			return logger_executor(this, spdlog::level::critical, fmt, args...);
		}

		void set_level(spdlog::level::level_enum lvl)
		{
			this->_logger->set_level(lvl);
		}

		void set_pattern(std::string pattern, spdlog::pattern_time_type time_type = spdlog::pattern_time_type::local)
		{
			this->_logger->set_pattern(pattern, time_type);
		}

		void set_error_handler(spdlog::err_handler error_handler)
		{
			this->_logger->set_error_handler(error_handler);
		}

		void set_formatter(std::unique_ptr<spdlog::formatter> formatter)
		{
			this->_logger->set_formatter(std::move(formatter));
		}

		void flush_on(spdlog::level::level_enum lvl)
		{
			this->_logger->flush_on(lvl);
		}

		spdlog::level::level_enum flush_lvl()
		{
			return this->_logger->flush_level();
		}

		void set_context(std::vector<log_data> ctx)
		{
			_context_str = format_data(ctx);
		}

		void add_context(std::vector<log_data> ctx)
		{
			_context_str += format_data(ctx);
		}

		std::string format_data(std::vector<log_data> data)
		{
			std::string fmtData;
			for (auto& d : data)
				fmtData += d.format("\n\t{} = {}");
			return fmtData;
		}

		void log(spdlog::level::level_enum lvl, std::string msg)
		{
			 msg += _context_str;
			this->_logger->log(lvl, msg);
		}
	protected:
		std::unique_ptr<spdlog::logger> _logger;
		std::string _context_str;
	};

	/**
	 * Factory for loggers using spdlog.
	*/
	class LogFactory
	{
	public:
		using TSharedLogger = std::shared_ptr<structured_logger>;
		using TSharedSink = std::shared_ptr<spdlog::sinks::sink>;
		
		/**
		 * Get instance of logger.
		 * 
		 * @param name name of logger (used as prefix)
		 * @param fileName logfile (uses default logfile when omitted)
		 * @return instance of shared spdlog::logger
		*/
		TSharedLogger Get(std::string name, LogLevel logLevel, std::string fileName);
		TSharedLogger Get(std::string name, LogLevel logLevel);
		TSharedLogger Get(std::string name, std::string fileName);
		TSharedLogger Get(std::string name);

		/**
		 * Create instance of logger.
		 * 
		 * @param name name of logger (used as prefix)
		 * @param level log level
		 * @param fileName filename of log file (uses default logfile when omitted)
		 * @return instance of shared spdlog::logger
		*/
		TSharedLogger Create(std::string name, LogLevel loglevel, std::string fileName);
		TSharedLogger Create(std::string name, LogLevel logLevel);
		TSharedLogger Create(std::string name, std::string fileName);
		TSharedLogger Create(std::string name);

		void SetDefaultLevel(LogLevel level);
		LogLevel GetDefaultLevel();

		void SetDefaultFile(std::string fileName);
		std::string GetDefaultFile();

		void SetLogTarget(LoggerTarget target);
		LoggerTarget GetLogTarget();
	private:
		std::map<std::string, TSharedLogger> _Logs = {};
		std::map<std::string, TSharedSink> _Sinks = {};
		std::map<std::string, std::string> _LogFileNames = {};
		LoggerTarget _LogTarget = LoggerTarget::FILE;

		LogLevel DefaultLogLevel = DEFAULT_LOG_LEVEL;
		std::string DefaultLogFile = DEFAULT_LOG_FILENAME;
	};
}