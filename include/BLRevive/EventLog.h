#pragma once
#include <SdkHeaders.h>
#include <fstream>

namespace BLRE
{
	class EventLog
	{
	public:
		int level = 0;

		EventLog();

		void log(UObject* Object, UFunction* Function, bool isEvent = false);
	private:
		std::string logFilePath;
		std::ofstream logFile;
	};
}
