#pragma once
#include <SdkHeaders.h>
#include <BLRevive/Component.h>
#include <steam/steam_gameserver.h>
#include <thread>
#include <BLRevive/Detours/FunctionDetour.h>

namespace BLRE
{
	class UserAuth : public Component
	{
	public:
		UserAuth() : Component("UserAuth") {}

		/**
		 * @brief initialize the user authentication component
		*/
		void Init();

		static void InitializeSteamApi();

	private:
		std::thread m_SteamCallbackThread;
		BLRE::Detours::FunctionDetour* dtFoxGame_PostLogin = nullptr;
		BLRE::Detours::FunctionDetour* dtServerNotifyPCSpawned = nullptr;
		AAccessControl* accessControl = nullptr;

		void SteamCallbackThread();

		void SetupBanList(AAccessControl* ac);

		const char* UnserializeNetString(FString string);
		void DETOUR_CB_CLS(ServerAuthenticateTicket, AFoxPC, ServerAuthenticateTicket);
		void DETOUR_CB_CLS(ServerNotifyPCSpawned, AFoxPC, ServerNotifyPCSpawned);
		void DETOUR_CB_CLS(AAccessControl_PostBeginPlay, AAccessControl, PostBeginPlay);
	};
}