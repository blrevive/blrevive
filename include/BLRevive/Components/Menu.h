#pragma once
#include <string>
#include <functional>
#include <map>

namespace BLRE::UI
{
	/**
	 * Helper function for trim whitespace from string
	 * 
	 * @param str input string
	 * @param whitespace characters to trim
	 * @return trimmed string
	*/
	std::string trim(const std::string& str, const std::string& whitespace = " \t");

	class Menu;

	/**
	 * A command which can be added to menus.
	*/
	class Command
	{
	public:
		// associated menu
		Menu* parent;

		// command handler
		std::function<void(std::string)> clb;
	};

	/**
	 * A menu for console handling command inputs and submenus.
	*/
	class Menu
	{
		using TCallback = std::function<void(std::string)>;
		using CommandMap = std::map<std::string, TCallback>;
		using MenuMap = std::map<std::string, Menu>;
	protected:
		// parent menu
		Menu* parent;
		// registered commands
		CommandMap commands;
		// registered submenus
		MenuMap menus;

	public:
		Menu(CommandMap cmds = CommandMap(), MenuMap menus = MenuMap()) :
			commands(cmds),
			menus(menus),
			parent(nullptr)
		{}

		Menu(Menu* parent, CommandMap cmds = CommandMap(), MenuMap menus = MenuMap()) :
			commands(cmds),
			menus(menus),
			parent(parent) {}

		/**
		 * Get count of commands.
		 * 
		 * @return command count
		*/
		int CmdCount() { return commands.size(); }

		/**
		 * Get count of submenus.
		 * 
		 * @return submenu count
		*/
		int MenuCount() { return menus.size(); }

		/**
		 * Check if this menu is the root menu.
		 * 
		 * @return menu is root
		*/
		bool IsRoot() { return parent == nullptr; }

		/**
		 * Get parent menu.
		 * 
		 * @return parent menu
		*/
		Menu* GetParent() { return parent; }

		/**
		 * Get root menu.
		 * 
		 * @return root menu
		*/
		Menu* RootMenu();

		/**
		 * Add a command to this menu.
		 * 
		 * @param name name of the command
		 * @param clb command handler
		 * @return command was added
		*/
		bool AddCommand(std::string name, TCallback clb);

		/**
		 * Add submenu to this menu.
		 * 
		 * @param name name of submenu
		 * @param menu submenu instance
		 * @return submenu reference
		*/
		Menu* AddMenu(std::string name, Menu menu);

		/**
		 * Get submenu.
		 * 
		 * @param name name of submenu
		 * @return submenu reference or nullptr if not found
		*/
		Menu* GetMenu(std::string name);

		/**
		 * Get command.
		 * 
		 * @param name name of command
		 * @return command handler or nullptr if not found
		*/
		TCallback* GetCommand(std::string name);

		/**
		 * Handle execution of a command.
		 * 
		 * @param cmd command line
		 * @param recursive handle commands from submenus
		*/
		void Exec(std::string cmd, bool recursive = true);
	};
}