#pragma once
#include <BLRevive/Component.h>

namespace BLRE::UI
{
    // forward declaration of BLRE::UI::Menu
    class Menu;

    /**
     * Helper class for dealing with console inputs
    */
    class Console : public Component
    {
    public:
        Console();

        /**
         * Initialize the console (create or attach to a console window)
        */
        void Init();

        /**
         * Handle console inputs (line by line)
        */
        void Run();

        void Exit();

        static void AttachOrCreate();
    protected:
        // root menu entry
        Menu* Root;
        bool shouldExit = false;
    };
}