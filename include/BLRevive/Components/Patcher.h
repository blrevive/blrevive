#pragma once
#include "nlohmann/json_fwd.hpp"
#include <BLRevive/Component.h>
#include <nlohmann/json.hpp>

class UClass;
class UProperty;
class UObject;
class UFoxDataProvider_GearInfo;
class UFoxDataProvider_ModBase;

namespace BLRE
{
	class BLRevive;

	class Patcher : Component
	{
	public:
		Patcher(BLRevive* blre) : Component("Patcher") 
		{
			this->blre = blre;
		}

		void LoadPatchConfig(std::string filePath = "");
		void ApplyClassPatches(const nlohmann::json& patches);
		void ApplyGearPatches(const nlohmann::json& patches);
		nlohmann::json& GetPatchConfig();
	private:
		std::unordered_map<std::string, UProperty*> GetClassConfigurableProps(UClass* cls);
		void MapClassDefaultProperties(UClass* cls, const nlohmann::json& properties);
		void MapGearProperties(UFoxDataProvider_GearInfo* gear, const nlohmann::json& properties);
		void MapModProperties(UFoxDataProvider_ModBase* mod, const nlohmann::json& properties);

		bool GetPatchFileFromServer(nlohmann::json& patchFile);
		bool GetPatchFileFromResources(nlohmann::json& patchFile);
		bool GetPatchFileFromDisk(std::string path, nlohmann::json& patchFile);
	private:
		std::map<UClass*, std::unordered_map<std::string, UProperty*>> ClassConfigPropsCache = {};
		BLRevive* blre;
		nlohmann::json patchConfig;
	};
}